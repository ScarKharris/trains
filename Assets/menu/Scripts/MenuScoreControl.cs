﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class MenuScoreControl : MonoBehaviour {

    public Image star_enable;
    public Image star_disable;
    public Image panel_disable;
    public Image panel_enable;
    public Image image_status_play;
    public Image image_status_compl;

	public Text level1_stepsCountText;
	public Image level1_star1Image;
	public Image level1_star2Image;
	public Image level1_star3Image;
    public Image level1_statusImage;


	public Text level2_stepsCountText;
	public Image level2_star1Image;
	public Image level2_star2Image;
	public Image level2_star3Image;
    public Image level2_blockImage;
    public Image level2_photoImage;
    public Image level2_statusImage;
    public Image level2_panelImage;


    public Text level3_stepsCountText;
    public Image level3_star1Image;
    public Image level3_star2Image;
    public Image level3_star3Image;
    public Image level3_blockImage;
    public Image level3_photoImage;
    public Image level3_statusImage;
    public Image level3_panelImage;

    public Text level4_stepsCountText;
    public Image level4_star1Image;
    public Image level4_star2Image;
    public Image level4_star3Image;
    public Image level4_blockImage;
    public Image level4_photoImage;
    public Image level4_statusImage;
    public Image level4_panelImage;

    public Text level5_stepsCountText;
    public Image level5_star1Image;
    public Image level5_star2Image;
    public Image level5_star3Image;
    public Image level5_blockImage;
    public Image level5_photoImage;
    public Image level5_statusImage;
    public Image level5_panelImage;

    public Text level6_stepsCountText;
    public Image level6_star1Image;
    public Image level6_star2Image;
    public Image level6_star3Image;
    public Image level6_blockImage;
    public Image level6_photoImage;
    public Image level6_statusImage;
    public Image level6_panelImage;

    public Text level7_stepsCountText;
    public Image level7_star1Image;
    public Image level7_star2Image;
    public Image level7_star3Image;
    public Image level7_blockImage;
    public Image level7_photoImage;
    public Image level7_statusImage;
    public Image level7_panelImage;
	// Use this for initialization
    void Start()
    {
        Color color_active = new Color();
        color_active.r = 1f;
        color_active.g = 0.9f;
        color_active.b = 0.6f;
        color_active.a = 1f;

        Color color_active_star = new Color();
        color_active_star.r = 1f;
        color_active_star.g = 1f;
        color_active_star.b = 1f;
        color_active_star.a = 1f;

        int level1_stepsCount = PlayerPrefs.GetInt("level1_stepsCount");
        if (level1_stepsCount == 0)
        {
            level1_stepsCountText.text = "--";

        }
        else
        {
            level1_stepsCountText.text = level1_stepsCount.ToString();

            if (Level1_starsData.countStepStar3 >= level1_stepsCount)
            {
                level1_star1Image.sprite = star_enable.sprite;
                level1_star2Image.sprite = star_enable.sprite;
                level1_star3Image.sprite = star_enable.sprite;
                level1_statusImage.sprite = image_status_compl.sprite;

            }
            else if (Level1_starsData.countStepStar2 >= level1_stepsCount)
            {
                level1_star1Image.sprite = star_enable.sprite;
                level1_star2Image.sprite = star_enable.sprite;
                level1_star3Image.sprite = star_disable.sprite;
            }
            else if (Level1_starsData.countStepStar1 >= level1_stepsCount)
            {
                level1_star1Image.sprite = star_enable.sprite;
                level1_star2Image.sprite = star_disable.sprite;
                level1_star3Image.sprite = star_disable.sprite;
            }
            else
            {
                level1_star1Image.sprite = star_disable.sprite;
                level1_star2Image.sprite = star_disable.sprite;
                level1_star3Image.sprite = star_disable.sprite;
            }

       

            if (Level1_starsData.countStepStar2 >= level1_stepsCount)
            {
               
                GameObject button2 = GameObject.Find("button_level_2");
                level2_blockImage.enabled = false;
                level2_photoImage.enabled = true;
                level2_statusImage.enabled = true;
                level2_panelImage.sprite = panel_enable.sprite;
                
                level2_star1Image.color = color_active_star;
                level2_star2Image.color = color_active_star;
                level2_star3Image.color = color_active_star;

               
                button2.GetComponent<Button>().enabled = true;
                level2_panelImage.color = color_active;      

                int level2_stepsCount = PlayerPrefs.GetInt("level2_stepsCount");
                if (level2_stepsCount == 0)
                {
                    level2_stepsCountText.text = "--";
                }
                if (level2_stepsCount != 0)
                {


                    if (Level2_starsData.countStepStar3 >= level2_stepsCount)
                    {
                        level2_statusImage.sprite = image_status_compl.sprite;
                        level2_star1Image.sprite = star_enable.sprite;
                        level2_star2Image.sprite = star_enable.sprite;
                        level2_star3Image.sprite = star_enable.sprite;
                    }
                    else if (Level2_starsData.countStepStar2 >= level2_stepsCount)
                    {
                        level2_star1Image.sprite = star_enable.sprite;
                        level2_star2Image.sprite = star_enable.sprite;
                        level2_star3Image.sprite = star_disable.sprite;
                    }
                    else if (Level2_starsData.countStepStar1 >= level2_stepsCount)
                    {
                        level2_star1Image.sprite = star_disable.sprite;
                        level2_star2Image.sprite = star_disable.sprite;
                        level2_star3Image.sprite = star_disable.sprite;
                    }
                    else
                    {
                        level2_star1Image.sprite = star_disable.sprite;
                        level2_star2Image.sprite = star_disable.sprite;
                        level2_star3Image.sprite = star_disable.sprite;
                    }

                    level2_stepsCountText.text = level2_stepsCount.ToString();



                    //----------------------------------------третий уровень-------------------------------


                    if (Level2_starsData.countStepStar2 >= level2_stepsCount)
                    {
                         GameObject p_button3 = GameObject.Find("panel_button3");
                         GameObject button3 = GameObject.Find("button_level_3");
                        level3_blockImage.enabled = false;
                        level3_photoImage.enabled = true;
                        level3_statusImage.enabled = true;
                        level3_panelImage.sprite = panel_enable.sprite;
                        level3_panelImage.color = color_active;
                        level3_star1Image.color = color_active_star;
                        level3_star2Image.color = color_active_star;
                        level3_star3Image.color = color_active_star;

                     //   if (p_button2 && button2)
                        {
                            button3.GetComponent<Button>().enabled = true;
                     //       p_button2.SetActive(false);

                        }
                        int level3_stepsCount = PlayerPrefs.GetInt("level3_stepsCount");
                        if (level3_stepsCount == 0)
                        {
                            level3_stepsCountText.text = "--";
                        }
                        if (level3_stepsCount != 0)
                        {

                            if (Level3_starsData.countStepStar3 >= level3_stepsCount)
                            {
                                level3_statusImage.sprite = image_status_compl.sprite;
                                level3_star1Image.sprite = star_enable.sprite;
                                level3_star2Image.sprite = star_enable.sprite;
                                level3_star3Image.sprite = star_enable.sprite;
                            }
                            else if (Level3_starsData.countStepStar2 >= level3_stepsCount)
                            {
                                level3_star1Image.sprite = star_enable.sprite;
                                level3_star2Image.sprite = star_enable.sprite;
                                level3_star3Image.sprite = star_disable.sprite;
                            }
                            else if (Level3_starsData.countStepStar1 >= level3_stepsCount)
                            {
                                level3_star1Image.sprite = star_disable.sprite;
                                level3_star2Image.sprite = star_disable.sprite;
                                level3_star3Image.sprite = star_disable.sprite;
                            }
                            else
                            {
                                level3_star1Image.sprite = star_disable.sprite;
                                level3_star2Image.sprite = star_disable.sprite;
                                level3_star3Image.sprite = star_disable.sprite;
                            }

                            level3_stepsCountText.text = level3_stepsCount.ToString();
                        }

                    //----------------------------------------четвертый уровень-------------------------------


                        if (Level3_starsData.countStepStar2 >= level3_stepsCount)
                        {
                             GameObject p_button4 = GameObject.Find("panel_button4");
                             GameObject button4 = GameObject.Find("button_level_4");
                            level4_blockImage.enabled = false;
                            level4_photoImage.enabled = true;
                            level4_statusImage.enabled = true;
                            level4_panelImage.sprite = panel_enable.sprite;
                            level4_panelImage.color = color_active;
                            level4_star1Image.color = color_active_star;
                            level4_star2Image.color = color_active_star;
                            level4_star3Image.color = color_active_star;

                            button4.GetComponent<Button>().enabled = true;

                            int level4_stepsCount = PlayerPrefs.GetInt("level4_stepsCount");
                            if (level4_stepsCount == 0)
                            {
                                level4_stepsCountText.text = "--";
                            }
                            if (level4_stepsCount != 0)
                            {

                                if (Level4_starsData.countStepStar3 >= level4_stepsCount)
                                {
                                    level4_statusImage.sprite = image_status_compl.sprite;
                                    level4_star1Image.sprite = star_enable.sprite;
                                    level4_star2Image.sprite = star_enable.sprite;
                                    level4_star3Image.sprite = star_enable.sprite;
                                }
                                else if (Level4_starsData.countStepStar2 >= level4_stepsCount)
                                {
                                    level3_star1Image.sprite = star_enable.sprite;
                                    level3_star2Image.sprite = star_enable.sprite;
                                    level3_star3Image.sprite = star_disable.sprite;
                                }
                                else if (Level4_starsData.countStepStar1 >= level4_stepsCount)
                                {
                                    level4_star1Image.sprite = star_disable.sprite;
                                    level4_star2Image.sprite = star_disable.sprite;
                                    level4_star3Image.sprite = star_disable.sprite;
                                }
                                else
                                {
                                    level4_star1Image.sprite = star_disable.sprite;
                                    level4_star2Image.sprite = star_disable.sprite;
                                    level4_star3Image.sprite = star_disable.sprite;
                                }

                                level4_stepsCountText.text = level4_stepsCount.ToString();
                            }

                            //----------------------------------------пятый уровень-------------------------------


                            if (Level4_starsData.countStepStar2 >= level4_stepsCount)
                            {
                                GameObject button5 = GameObject.Find("button_level_5");
                                level5_blockImage.enabled = false;
                                level5_photoImage.enabled = true;
                                level5_statusImage.enabled = true;
                                level5_panelImage.sprite = panel_enable.sprite;
                                level5_panelImage.color = color_active;
                                level5_star1Image.color = color_active_star;
                                level5_star2Image.color = color_active_star;
                                level5_star3Image.color = color_active_star;

                                button5.GetComponent<Button>().enabled = true;

                                int level5_stepsCount = PlayerPrefs.GetInt("level5_stepsCount");
                                if (level5_stepsCount == 0)
                                {
                                    level5_stepsCountText.text = "--";
                                }
                                if (level5_stepsCount != 0)
                                {

                                    if (Level5_starsData.countStepStar3 >= level5_stepsCount)
                                    {
                                        level5_statusImage.sprite = image_status_compl.sprite;
                                        level5_star1Image.sprite = star_enable.sprite;
                                        level5_star2Image.sprite = star_enable.sprite;
                                        level5_star3Image.sprite = star_enable.sprite;
                                    }
                                    else if (Level5_starsData.countStepStar2 >= level5_stepsCount)
                                    {
                                        level5_star1Image.sprite = star_enable.sprite;
                                        level5_star2Image.sprite = star_enable.sprite;
                                        level5_star3Image.sprite = star_disable.sprite;
                                    }
                                    else if (Level5_starsData.countStepStar1 >= level5_stepsCount)
                                    {
                                        level5_star1Image.sprite = star_disable.sprite;
                                        level5_star2Image.sprite = star_disable.sprite;
                                        level5_star3Image.sprite = star_disable.sprite;
                                    }
                                    else
                                    {
                                        level5_star1Image.sprite = star_disable.sprite;
                                        level5_star2Image.sprite = star_disable.sprite;
                                        level5_star3Image.sprite = star_disable.sprite;
                                    }

                                    level5_stepsCountText.text = level5_stepsCount.ToString();
                                }
                                //----------------------------------------шестой уровень-------------------------------


                                if (Level5_starsData.countStepStar2 >= level5_stepsCount)
                                {
                                    GameObject button6 = GameObject.Find("button_level_6");
                                    level6_blockImage.enabled = false;
                                    level6_photoImage.enabled = true;
                                    level6_statusImage.enabled = true;
                                    level6_panelImage.sprite = panel_enable.sprite;
                                    level6_panelImage.color = color_active;
                                    level6_star1Image.color = color_active_star;
                                    level6_star2Image.color = color_active_star;
                                    level6_star3Image.color = color_active_star;

                                    button6.GetComponent<Button>().enabled = true;

                                    int level6_stepsCount = PlayerPrefs.GetInt("level6_stepsCount");
                                    if (level6_stepsCount == 0)
                                    {
                                        level6_stepsCountText.text = "--";
                                    }
                                    if (level6_stepsCount != 0)
                                    {

                                        if (Level6_starsData.countStepStar3 >= level6_stepsCount)
                                        {
                                            level6_statusImage.sprite = image_status_compl.sprite;
                                            level6_star1Image.sprite = star_enable.sprite;
                                            level6_star2Image.sprite = star_enable.sprite;
                                            level6_star3Image.sprite = star_enable.sprite;
                                        }
                                        else if (Level6_starsData.countStepStar2 >= level6_stepsCount)
                                        {
                                            level6_star1Image.sprite = star_enable.sprite;
                                            level6_star2Image.sprite = star_enable.sprite;
                                            level6_star3Image.sprite = star_disable.sprite;
                                        }
                                        else if (Level6_starsData.countStepStar1 >= level6_stepsCount)
                                        {
                                            level6_star1Image.sprite = star_disable.sprite;
                                            level6_star2Image.sprite = star_disable.sprite;
                                            level6_star3Image.sprite = star_disable.sprite;
                                        }
                                        else
                                        {
                                            level6_star1Image.sprite = star_disable.sprite;
                                            level6_star2Image.sprite = star_disable.sprite;
                                            level6_star3Image.sprite = star_disable.sprite;
                                        }

                                        level6_stepsCountText.text = level6_stepsCount.ToString();
                                    }
                                    //---------------------------------------- уровень 7-------------------------------


                                    if (Level6_starsData.countStepStar2 >= level6_stepsCount)
                                    {
                                        GameObject button7 = GameObject.Find("button_level_7");
                                        level7_blockImage.enabled = false;
                                        level7_photoImage.enabled = true;
                                        level7_statusImage.enabled = true;
                                        level7_panelImage.sprite = panel_enable.sprite;
                                        level7_panelImage.color = color_active;
                                        level7_star1Image.color = color_active_star;
                                        level7_star2Image.color = color_active_star;
                                        level7_star3Image.color = color_active_star;

                                        button7.GetComponent<Button>().enabled = true;

                                        int level7_stepsCount = PlayerPrefs.GetInt("level7_stepsCount");
                                        if (level7_stepsCount == 0)
                                        {
                                            level7_stepsCountText.text = "--";
                                        }
                                        if (level7_stepsCount != 0)
                                        {

                                            if (Level7_starsData.countStepStar3 >= level7_stepsCount)
                                            {
                                                level7_statusImage.sprite = image_status_compl.sprite;
                                                level7_star1Image.sprite = star_enable.sprite;
                                                level7_star2Image.sprite = star_enable.sprite;
                                                level7_star3Image.sprite = star_enable.sprite;
                                            }
                                            else if (Level7_starsData.countStepStar2 >= level7_stepsCount)
                                            {
                                                level7_star1Image.sprite = star_enable.sprite;
                                                level7_star2Image.sprite = star_enable.sprite;
                                                level7_star3Image.sprite = star_disable.sprite;
                                            }
                                            else if (Level7_starsData.countStepStar1 >= level7_stepsCount)
                                            {
                                                level7_star1Image.sprite = star_disable.sprite;
                                                level7_star2Image.sprite = star_disable.sprite;
                                                level7_star3Image.sprite = star_disable.sprite;
                                            }
                                            else
                                            {
                                                level7_star1Image.sprite = star_disable.sprite;
                                                level7_star2Image.sprite = star_disable.sprite;
                                                level7_star3Image.sprite = star_disable.sprite;
                                            }

                                            level7_stepsCountText.text = level7_stepsCount.ToString();
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }
 
	
	// Update is called once per frame
	void Update () {
	
	}


}
