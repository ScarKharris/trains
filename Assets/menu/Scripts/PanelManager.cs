﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class PanelManager : MonoBehaviour {

	public Animator initiallyOpen;
	public GameObject loadScreen1;
    public GameObject loadScreen2;
	public GameObject panelLevel;
    public GameObject panelLevel2;
	private int m_OpenParameterId;
	private Animator m_Open;
	private GameObject m_PreviouslySelected;

	const string k_OpenTransitionName = "Open";
	const string k_ClosedStateName = "Closed";
	void Start () {
		

	}
	public void OnEnable()
	{
		m_OpenParameterId = Animator.StringToHash (k_OpenTransitionName);

		if (initiallyOpen == null)
			return;

		OpenPanel(initiallyOpen);
	}

	public void OpenPanel (Animator anim)
	{
		if (m_Open == anim)
			return;

		anim.gameObject.SetActive(true);
		var newPreviouslySelected = EventSystem.current.currentSelectedGameObject;

		anim.transform.SetAsLastSibling();

		CloseCurrent();

		m_PreviouslySelected = newPreviouslySelected;

		m_Open = anim;
		m_Open.SetBool(m_OpenParameterId, true);

		GameObject go = FindFirstEnabledSelectable(anim.gameObject);

		SetSelected(go);
	}

	static GameObject FindFirstEnabledSelectable (GameObject gameObject)
	{
		GameObject go = null;
		var selectables = gameObject.GetComponentsInChildren<Selectable> (true);
		foreach (var selectable in selectables) {
			if (selectable.IsActive () && selectable.IsInteractable ()) {
				go = selectable.gameObject;
				break;
			}
		}
		return go;
	}

	public void CloseCurrent()
	{
		if (m_Open == null)
			return;

		m_Open.SetBool(m_OpenParameterId, false);
		SetSelected(m_PreviouslySelected);
		StartCoroutine(DisablePanelDeleyed(m_Open));
		m_Open = null;
	}

	IEnumerator DisablePanelDeleyed(Animator anim)
	{
		bool closedStateReached = false;
		bool wantToClose = true;
		while (!closedStateReached && wantToClose)
		{
			if (!anim.IsInTransition(0))
				closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

			wantToClose = !anim.GetBool(m_OpenParameterId);

			yield return new WaitForEndOfFrame();
		}

		if (wantToClose)
			anim.gameObject.SetActive(false);
	}

	private void SetSelected(GameObject go)
	{
		EventSystem.current.SetSelectedGameObject(go);
	}
	public void openLevel1 ()
	{
		panelLevel.SetActive (false);
        loadScreen1.SetActive(true);
		Application.LoadLevel ("level1");
	}
	public void openLevel2 ()
	{
        panelLevel.SetActive(false);
        loadScreen2.SetActive(true);
		Application.LoadLevel ("level2");
	}
    public void openLevel3()
    {
        panelLevel.SetActive(false);
        loadScreen2.SetActive(true);
        Application.LoadLevel("level3");
    }
    public void openLevel4()
    {
        panelLevel.SetActive(false);
        loadScreen2.SetActive(true);
        Application.LoadLevel("level4");
    }
    public void openLevel5()
    {
        panelLevel.SetActive(false);
        loadScreen2.SetActive(true);
        Application.LoadLevel("level5");
    }
    public void openLevel6()
    {
        panelLevel.SetActive(false);
        loadScreen2.SetActive(true);
        Application.LoadLevel("level6");
    }
    public void openLevel7()
    {
        panelLevel.SetActive(false);
        panelLevel2.SetActive(false);
        loadScreen2.SetActive(true);
        Application.LoadLevel("level7");
    }
}