﻿using UnityEngine;
using System.Collections;

public class TutorAnimEvent : MonoBehaviour {

    public GameObject panel_tutorTitle;
    public GameObject panel_tutorMoveMap;
    public GameObject panel_tutorZoomMap;
    public GameObject panel_tutorWagonMove;
	public GameObject panel_tutorWagon2Move;
	public GameObject panel_tutorPressHoldWagon;
	public GameObject panel_tutorTgtPoint;
	public GameObject panel_tutorPressFork;
	public GameObject panel_tutorPointGo;
	public GameObject panel_tutorHelpSnapShot;
	public GameObject panel_tutorConnectedWags;
	public GameObject handHelp;
	public GameObject pointLightHelp;
	public GameObject clickGrabber;
	private Animator animCamera;
    public GameObject handHelpMove;
    public GameObject handHelpZoom1;
    public GameObject handHelpZoom2;
	public GameObject pointLightTgt1;
	public GameObject pointLightTgt2;
	public Animator[] pointsLightGoAnim;
	public Animator photoButtonAnimator;
	public GameObject current_active_panel; // текущая активная панель меню
   // public bool fl_TutorZoom = false;
   // public bool fl_TutorMove = false;
   // public bool fl_TutorMoveWagon = false;
	private PointLightTgtTutor tgtPntScript;
	//private Wagon m_wagonScript;	// ссылка на скрипт вагона для первого объекта
	// Use this for initialization
	void Start () {
       // panel_tutorTitle = GameObject.Find("panel_tutorTitle");
      //  panel_tutorMoveMap = GameObject.Find("panel_tutorMoveMap");
        animCamera = gameObject.GetComponent<Animator>();
		//m_wagonScript =  wagon1.GetComponent<Wagon> ();
		//m_wagonScript.enabled = false;
        handHelp.SetActive(false);
	}

   public void on_nextButton_endTutorWelcomPanel()
    {
		panel_tutorTitle.SetActive(false);
		panel_tutorHelpSnapShot.SetActive(true);
		current_active_panel = panel_tutorHelpSnapShot;
		photoButtonAnimator.enabled = true;
      //  fl_TutorMove = true;
    }

	public void on_nextButton_helpSnapShot()
	{
		panel_tutorHelpSnapShot.SetActive(false);
		panel_tutorMoveMap.SetActive(true);
		current_active_panel = panel_tutorMoveMap;
		handHelpMove.GetComponent<Animator>().SetTrigger("MoveMap");
		animCamera.SetTrigger("MoveMap");
		photoButtonAnimator.SetTrigger ("endPBF");
	}

   public void on_nextButton_tutorMoveMap()
   {
       panel_tutorMoveMap.SetActive(false);
       panel_tutorZoomMap.SetActive(true);
		current_active_panel = panel_tutorZoomMap;
       handHelpMove.SetActive(false);
       handHelpZoom1.SetActive(true);
       handHelpZoom1.GetComponent<Animator>().SetTrigger("ZoomMap");
       handHelpZoom2.SetActive(true);
		handHelpZoom2.GetComponent<Animator>().SetTrigger("MoveMap");
       animCamera.SetTrigger("ZoomMap");
		photoButtonAnimator.enabled = false;
      // fl_TutorZoom = true;
      // fl_TutorMove = false;
   }
   public void on_nextButton_tutorZoomMap()
   {
       panel_tutorZoomMap.SetActive(false);
       handHelpZoom1.SetActive(false);
       handHelpZoom2.SetActive(false);
       panel_tutorPressHoldWagon.SetActive(true);
		current_active_panel = panel_tutorPressHoldWagon;
       //fl_TutorZoom = false;
       //fl_TutorMoveWagon = false;
       animCamera.SetTrigger("MoveWagon");
	   handHelp.SetActive(true);
	   pointLightHelp.SetActive (true);
   }
   public void on_nextButton_tutorMoveWagon()
   {
      // fl_TutorMoveWagon = false;
      // animCamera.enabled = false;
       //animCamera.SetTrigger("Final");
	   //panel_tutorWagon2Move.SetActive(false);    
   }

	public void on_tutorPressHoldWagon()	// после первого нажатия на вагон
	{
		panel_tutorPressHoldWagon.SetActive(false);
		panel_tutorTgtPoint.SetActive(true);
		current_active_panel = panel_tutorTgtPoint;
		pointLightTgt1.SetActive (true);
		pointLightTgt2.SetActive (true);
		animCamera.SetTrigger ("MoveToTgt");
       // Debug.Log("33");
	}

	public void on_nextButtonPointGo()
	{
		panel_tutorPointGo.SetActive(false);
		panel_tutorWagonMove.SetActive(true);
		current_active_panel = panel_tutorWagonMove;
        handHelp.SetActive(true);
		//m_wagonScript.enabled = true;
		foreach (Animator anim in pointsLightGoAnim)
		{
			anim.enabled = false;	// выключаем анимацию
			Vector3 vect = anim.transform.position;	// возвращаем точки на исходные позиции
			vect.y = 0.2f;
			anim.transform.position = vect;  
		}
        animCamera.SetTrigger("MoveWagon");
		clickGrabber.SetActive (false);
	}

	public void on_nextButton_TgtPoint()
	{
		panel_tutorTgtPoint.SetActive (false);
		panel_tutorPointGo.SetActive (true);
		current_active_panel = panel_tutorPointGo;
		pointLightTgt1.SetActive (false);
		pointLightTgt2.SetActive (false);
        handHelp.SetActive(false);
		animCamera.SetTrigger("MoveWagonSecond");
		foreach (Animator anim in pointsLightGoAnim)
		{
			anim.enabled = true;
		}
	}

	public void on_tutorWagonMove()	// после перемещения вагона
	{
		panel_tutorWagonMove.SetActive(false);
		panel_tutorPressFork.SetActive(true);
		current_active_panel = panel_tutorPressFork;
        animCamera.SetTrigger("MoveFork");
	}

	public void on_tutorSwitchFork()	// после переключения форка
	{
		panel_tutorPressFork.SetActive(false);
		panel_tutorWagonMove.SetActive(true);
		current_active_panel = panel_tutorWagonMove;
        animCamera.SetTrigger("MoveWagon");


    }

	public void on_tutorSecondSwitchFork()	// после второго переключения форка
	{
		panel_tutorPressFork.SetActive(false);
		panel_tutorWagon2Move.SetActive(true);
		current_active_panel = panel_tutorWagon2Move;
        animCamera.SetTrigger("MoveWagonSecond");

	}

	public void on_tutorMoveConnectedWags()	// после первого перемещения сцепленных вагонов
	{
		panel_tutorWagon2Move.SetActive(false);
		panel_tutorConnectedWags.SetActive (true);
		current_active_panel = panel_tutorConnectedWags;
	}

	public void on_nextButton_ConnectedWags()
	{
		panel_tutorConnectedWags.SetActive(false);
		panel_tutorWagon2Move.SetActive(true);
		current_active_panel = panel_tutorWagon2Move;
	}

	public void final()
	{
		panel_tutorWagon2Move.SetActive(false);
        animCamera.SetTrigger("Final");
	}
}
