﻿using UnityEngine;
using System.Collections;

public class Fork : MonoBehaviour {
	private float anglePoint; 
	private float angleWagon; 
	public Animator anim_switchFork;
	/*public GameObject switchForkRail1_1;
	public GameObject switchForkRail1_2;
	public GameObject switchForkRail2_1;
	public GameObject switchForkRail2_2;*/
	public GameObject[] forkRails;
	public Animator anim_switchLightWallFork;
	public byte switchIndex;
	protected BaseControl ctrl;
	private bool isEnabled;

	void Start ()
	{
		isEnabled = false;
	
		//GetComponent<Renderer>().material.color = Color.red;
		ctrl = GameObject.Find("gameControl").GetComponent<BaseControl>();
		//railCoupler = new RailCoupler[railCouplerObj.Length];
		//for (int i = 0; i < railCouplerObj.Length; i++)
		//	railCoupler[i] = railCouplerObj[i].GetComponent<RailCoupler>();
	}
	// при нажатии происходит переключение стрелок
	void OnMouseDown()
	{
		if (isEnabled) {
			//anim_rails.SetBool("forkOn", true);
		
			anim_switchFork.SetBool("switch_on", false);
			anim_switchLightWallFork.SetBool("switchLightWallFork",false);
			ctrl.switchFork (switchIndex);
			findChangePositionWagon();
			for (int i = 0; i < forkRails.Length; i=i+2)
			{
				if(forkRails[i] != null )
					forkRails[i].SetActive(true);
				if(forkRails[i + 1] != null )
					forkRails[i + 1].SetActive(false);
			}
		//	ctrl = GameObject.Find("gameControl").GetComponent<MainControl>();
			//ctrl.switchFork (switchIndex);
			isEnabled = false;
//			GetComponent<Renderer>().material.color = Color.blue;
		} else { 
			//anim_rails.SetBool("forkOn", false);
			anim_switchFork.SetBool("switch_on", true);
			ctrl.switchFork (switchIndex);
			anim_switchLightWallFork.SetBool("switchLightWallFork",true);
			findChangePositionWagon();
			for (int i = 0; i < forkRails.Length; i=i+2)
			{
				if(forkRails[i] != null )
					forkRails[i].SetActive(false);
				if(forkRails[i + 1] != null )
					forkRails[i + 1].SetActive(true);
			}

			//	ctrl = GameObject.Find("gameControl").GetComponent<MainControl>();
		//	ctrl.switchFork (switchIndex);
			isEnabled = true;
//			GetComponent<Renderer>().material.color = Color.red;
		}
		/*for (int i = 0; i < railCoupler.Length; i++)
			railCoupler[i].startProcess ();*/
	}
	protected void changePositionWagon(GameObject wagon,GameObject switchForkRail1,GameObject switchForkRail2)
	{
		Transform point1 = null;
		Transform point2 = null;
		//поиск точки(дочерний объект путей)
		foreach(Transform child in switchForkRail1.transform)
			point1 = child;
		foreach(Transform child in switchForkRail2.transform)
			point2 = child;

		Transform pointBuf;
		if (!switchForkRail1.activeSelf && switchForkRail2.activeSelf) {
			pointBuf = point1;
			point1 = point2;
			point2 = pointBuf;
		}

		if (wagon.transform.position == point1.position)
		{
			PointS point  = ctrl.findPointSByVector3(point2.position);
			wagon.GetComponent<Wagon>().setCurPoints(point);
			angleWagon = wagon.transform.rotation.eulerAngles.y;

			wagon.transform.position = point2.position;
			wagon.GetComponent<Wagon> ().checkWagonTgt (); //проверка установки вагона в целевую точку, так как перемещение вагона на рельсе форка просходит здесь
			wagon.transform.rotation = Quaternion.Euler (270, - point2.localEulerAngles.x, 0);
		}
	}
	virtual public void findChangePositionWagon()
	{

		GameObject[] wagonsAll = GameObject.FindGameObjectsWithTag ("wagon");
		for (int i = 0; i<wagonsAll.Length; i++) {
			for (int j = 0; j < forkRails.Length; j=j+2)
			{
				if (forkRails[j] != null && forkRails[j + 1] != null )
					changePositionWagon(wagonsAll[i],forkRails[j], forkRails[j + 1]);
			}
		}

	}
}

