﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level4Control : BaseControl {
	private byte checkedForksRound;
	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected void init()
	{
		initTables ();		
		// ставим вагончики в исходную позицию

		wagons[0].transform.position = routeTable[0].curPoint;
		wagons[0].GetComponent<Wagon>().setCurPoints (routeTable[0]);
		wagons[1].transform.position = routeTable[4].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[4]);
		wagons[2].transform.position = routeTable[10].curPoint;
		wagons[2].GetComponent<Wagon>().setCurPoints (routeTable[10]);

		wagons[1].transform.rotation = Quaternion.Euler (270, 90f, 0);
		wagons[2].transform.rotation = Quaternion.Euler (270, 90f, 0);
		checkedForksRound = 0;

	}

	override public void switchFork(byte switchForkIndex)
	{	
		switch (checkedForksRound) 
		{
		case 0:
			forkSwitch1();
			checkedForksRound = 1;
			break;
		case 1:
			forkSwitch2();
			checkedForksRound = 2;
			break;
		case 2:
			forkSwitch3();
			checkedForksRound = 3;
			break;
		case 3:
			forkSwitch4();
			checkedForksRound = 0;
			break;
		}
	}

	private void forkSwitch1()
	{
		routeTable[1].pointB = Const.NULLp;
		routeTable[4].pointB = routeTable[3].curPoint;

		updatePointsWagon (routeTable [1].curPoint, 1);
		updatePointsWagon (routeTable [4].curPoint, 4);
	}

	private void forkSwitch2()
	{
		routeTable[4].pointB = Const.NULLp;
		routeTable[6].pointA = routeTable[5].curPoint ;

		updatePointsWagon (routeTable [4].curPoint, 4);
		updatePointsWagon (routeTable [6].curPoint, 6);
	}

	private void forkSwitch3()
	{
		routeTable[6].pointA = Const.NULLp;
		routeTable[10].pointA = routeTable[9].curPoint ;

		updatePointsWagon (routeTable [6].curPoint, 6);
		updatePointsWagon (routeTable [10].curPoint, 10);
	}

	private void forkSwitch4()
	{
		routeTable[10].pointA = Const.NULLp;
		routeTable[1].pointB = routeTable[2].curPoint;

		updatePointsWagon (routeTable [10].curPoint, 10);
		updatePointsWagon (routeTable [1].curPoint, 1);
	}
}
