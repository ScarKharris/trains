﻿using UnityEngine;
using System.Collections;

public class Level2Control : BaseControl {

	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// переключение стрелки switchPoint - точка, на которой находится стрелка
	override public void switchFork(byte switchForkIndex)
	{	
		switch (switchForkIndex) {
		case 0:
			if (checkedForks[switchForkIndex])
				disableForkSwitch1();
			else
				enableForkSwitch1();
			break;
		case 1:
			if (checkedForks[switchForkIndex])
				disableForkSwitch2();
			else
				enableForkSwitch2();
			break;
		}
		checkedForks [switchForkIndex] = !checkedForks [switchForkIndex];
	}
	
	protected void init()
	{
		initTables ();		
		// ставим вагончики в исходную позицию
		wagons[0].transform.position = routeTable[0].curPoint;
		wagons[0].GetComponent<Wagon>().setCurPoints (routeTable[0]);
		wagons[1].transform.position = routeTable[1].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[1]);
		wagons[2].transform.position = routeTable[13].curPoint;
		wagons[2].GetComponent<Wagon>().setCurPoints(routeTable[13]);
		wagons[3].transform.position = routeTable[8].curPoint;
		wagons[3].GetComponent<Wagon>().setCurPoints(routeTable[8]);
		wagons[4].transform.position = routeTable[9].curPoint;
		wagons[4].GetComponent<Wagon>().setCurPoints(routeTable[9]);
		wagons[5].transform.position = routeTable[17].curPoint;
		wagons[5].GetComponent<Wagon>().setCurPoints(routeTable[17]);
		
		checkedForks = new bool[2];
		checkedForks [0] = true;
		checkedForks [1] = true;
	}
	
	private void enableForkSwitch1()
	{
		
		routeTable [3].pointB = routeTable[2].curPoint;
		routeTable [1].pointA = routeTable [2].curPoint;
		routeTable [10].pointB = routeTable [9].curPoint;
		routeTable [8].pointA = routeTable [9].curPoint;
		
		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[1].curPoint,1);
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[8].curPoint,8);
		
		//		GameObject.Find("TrafficLight 5/Sphere Green").GetComponent<Renderer>().material = matGreen;
		//		GameObject.Find("TrafficLight 5/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
		//		GameObject.Find ("TrafficLight 5/Sphere Green/Point light").SetActive (true);
		//		GameObject.Find("TrafficLight 5/Sphere Red").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 5/Sphere Red/Point light").SetActive (false);
		
		//	GameObject.Find("TrafficLight 4/Sphere Green").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 4/Sphere Green/Point light").SetActive (false);
		//	GameObject.Find("TrafficLight 4/Sphere Red").GetComponent<Renderer>().material  = matRed;
		//	GameObject.Find("TrafficLight 4/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
		//		GameObject.Find ("TrafficLight 4/Sphere Red/Point light").SetActive (true);
		
		//	GameObject.Find("TrafficLight 3/Sphere Green").GetComponent<Renderer>().material  = matGreen;
		//	GameObject.Find("TrafficLight 3/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
		//		GameObject.Find ("TrafficLight 3/Sphere Green/Point light").SetActive (true);
		//	GameObject.Find("TrafficLight 3/Sphere Red").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 3/Sphere Red/Point light").SetActive (false);
	}
	
	private void disableForkSwitch1()
	{
		routeTable [3].pointB = Const.NULLp;
		routeTable [1].pointA = routeTable [20].curPoint;
		routeTable [10].pointB = routeTable [20].curPoint;
		routeTable [8].pointA = routeTable [21].curPoint;
		
		
		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[1].curPoint,1);
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[8].curPoint,8);
		
		//		GameObject.Find("TrafficLight 5/Sphere Green").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 5/Sphere Green/Point light").SetActive (false);
		//		GameObject.Find("TrafficLight 5/Sphere Red").GetComponent<Renderer>().material  = matRed;
		//		GameObject.Find("TrafficLight 5/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
		//		GameObject.Find ("TrafficLight 5/Sphere Red/Point light").SetActive (true);
		
		//	GameObject.Find("TrafficLight 4/Sphere Green").GetComponent<Renderer>().material  = matGreen;
		//	GameObject.Find("TrafficLight 4/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
		//		GameObject.Find ("TrafficLight 4/Sphere Green/Point light").SetActive (true);
		//	GameObject.Find("TrafficLight 4/Sphere Red").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 4/Sphere Red/Point light").SetActive (false);
		
		//	GameObject.Find("TrafficLight 3/Sphere Green").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 3/Sphere Green/Point light").SetActive (false);
		//	GameObject.Find("TrafficLight 3/Sphere Red").GetComponent<Renderer>().material  = matRed;
		//	GameObject.Find("TrafficLight 3/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
		//		GameObject.Find ("TrafficLight 3/Sphere Red/Point light").SetActive (true);
	}
	
	private void enableForkSwitch2()
	{	
		routeTable [10].pointA = Const.NULLp;
		routeTable [16].pointB = routeTable [15].curPoint;
		routeTable [14].pointA = routeTable [15].curPoint;
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[16].curPoint,16);
		updatePointsWagon(routeTable[14].curPoint,14);
		
		//	GameObject.Find("TrafficLight 2/Sphere Green").GetComponent<Renderer>().material  = matGreen;
		//	GameObject.Find("TrafficLight 2/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
		//		GameObject.Find ("TrafficLight 2/Sphere Green/Point light").SetActive (true);
		//	GameObject.Find("TrafficLight 2/Sphere Red").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 2/Sphere Red/Point light").SetActive (false);
		
		//	GameObject.Find("TrafficLight 1/Sphere Green").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 1/Sphere Green/Point light").SetActive (false);
		//	GameObject.Find("TrafficLight 1/Sphere Red").GetComponent<Renderer>().material  = matRed;
		//	GameObject.Find("TrafficLight 1/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
		//		GameObject.Find ("TrafficLight 1/Sphere Red/Point light").SetActive (true);
	}
	
	private void disableForkSwitch2()
	{
		routeTable [10].pointA = routeTable [19].curPoint;
		routeTable [16].pointB = routeTable [19].curPoint;
		routeTable [14].pointA = Const.NULLp;
		
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[16].curPoint,16);
		updatePointsWagon(routeTable[14].curPoint,14);
		
		//	GameObject.Find("TrafficLight 2/Sphere Green").GetComponent<Renderer>().material  = matOff;
		
		//		GameObject.Find ("TrafficLight 2/Sphere Green/Point light").SetActive (false);
		//	GameObject.Find("TrafficLight 2/Sphere Red").GetComponent<Renderer>().material  = matRed;
		//	GameObject.Find("TrafficLight 2/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
		//		GameObject.Find ("TrafficLight 2/Sphere Red/Point light").SetActive (true);
		
		//	GameObject.Find("TrafficLight 1/Sphere Green").GetComponent<Renderer>().material  = matGreen;
		//	GameObject.Find("TrafficLight 1/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
		//		GameObject.Find ("TrafficLight 1/Sphere Green/Point light").SetActive (true);
		//	GameObject.Find("TrafficLight 1/Sphere Red").GetComponent<Renderer>().material  = matOff;
		//		GameObject.Find ("TrafficLight 1/Sphere Red/Point light").SetActive (false);
	}

}
