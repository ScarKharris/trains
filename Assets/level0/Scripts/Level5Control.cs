﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level5Control : BaseControl {

	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected void init()
	{
		initTables ();
		wagons[0].transform.position = routeTable[36].curPoint;
		wagons[0].GetComponent<Wagon>().setCurPoints (routeTable[36]);
		wagons[1].transform.position = routeTable[4].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[4]);
		wagons[2].transform.position = routeTable[5].curPoint;
		wagons[2].GetComponent<Wagon>().setCurPoints (routeTable[5]);
		wagons[3].transform.position = routeTable[37].curPoint;
		wagons[3].GetComponent<Wagon>().setCurPoints (routeTable[37]);
		wagons[4].transform.position = routeTable[10].curPoint;
		wagons[4].GetComponent<Wagon>().setCurPoints (routeTable[10]);
		wagons[5].transform.position = routeTable[38].curPoint;
		wagons[5].GetComponent<Wagon>().setCurPoints (routeTable[38]);
		wagons[6].transform.position = routeTable[15].curPoint;
		wagons[6].GetComponent<Wagon>().setCurPoints (routeTable[15]);
		wagons[7].transform.position = routeTable[39].curPoint;
		wagons[7].GetComponent<Wagon>().setCurPoints (routeTable[39]);
		checkedForks = new bool[1];
		checkedForks [0] = true;
	}

	override public void switchFork(byte switchForkIndex)
	{	
		if (checkedForks [0])
			disableForkSwitch1 ();
		else
			enableForkSwitch1 ();
		checkedForks [0] = !checkedForks [0];
	}

	private void enableForkSwitch1()
	{
		routeTable [1].pointB = routeTable [2].curPoint;
		routeTable [3].pointA = routeTable [2].curPoint;
		routeTable [29].pointB = Const.NULLp;
		routeTable [6].pointB = routeTable [7].curPoint;
		routeTable [28].pointA = Const.NULLp;
		routeTable [9].pointA = routeTable [8].curPoint;
		routeTable [31].pointB = Const.NULLp;
		routeTable [30].pointA = Const.NULLp;
		routeTable [33].pointB = Const.NULLp;
		routeTable [32].pointA = Const.NULLp;
		routeTable [16].pointB = routeTable [17].curPoint;
		routeTable [19].pointA = routeTable [18].curPoint;
		routeTable [35].pointB = Const.NULLp;
		routeTable [34].pointA = Const.NULLp;
		routeTable [22].pointB = routeTable [23].curPoint;
		routeTable [24].pointA = routeTable [23].curPoint;

		updatePointsWagon (routeTable [1].curPoint, 1);
		updatePointsWagon (routeTable [3].curPoint, 3);
		updatePointsWagon (routeTable [29].curPoint, 29);
		updatePointsWagon (routeTable [6].curPoint, 6);
		updatePointsWagon (routeTable [28].curPoint, 28);
		updatePointsWagon (routeTable [9].curPoint, 9);
		updatePointsWagon (routeTable [31].curPoint, 31);
		updatePointsWagon (routeTable [30].curPoint, 30);
		updatePointsWagon (routeTable [33].curPoint, 33);
		updatePointsWagon (routeTable [32].curPoint, 32);
		updatePointsWagon (routeTable [16].curPoint, 16);
		updatePointsWagon (routeTable [19].curPoint, 19);
		updatePointsWagon (routeTable [35].curPoint, 35);
		updatePointsWagon (routeTable [34].curPoint, 34);
		updatePointsWagon (routeTable [22].curPoint, 22);
		updatePointsWagon (routeTable [24].curPoint, 24);
	}

	private void disableForkSwitch1()
	{
		routeTable [1].pointB = Const.NULLp;
		routeTable [3].pointA = routeTable [29].curPoint;
		routeTable [29].pointB = routeTable [3].curPoint;
		routeTable [6].pointB = routeTable [28].curPoint;
		routeTable [28].pointA = routeTable [6].curPoint;
		routeTable [9].pointA = routeTable [31].curPoint;
		routeTable [31].pointB = routeTable [9].curPoint;
		routeTable [30].pointA = routeTable [33].curPoint;
		routeTable [33].pointB = routeTable [30].curPoint;
		routeTable [32].pointA = routeTable [16].curPoint;
		routeTable [16].pointB = routeTable [32].curPoint;
		routeTable [19].pointA = routeTable [35].curPoint;
		routeTable [35].pointB = routeTable [19].curPoint;
		routeTable [34].pointA = routeTable [22].curPoint;
		routeTable [22].pointB = routeTable [34].curPoint;
		routeTable [24].pointA = Const.NULLp;

		updatePointsWagon (routeTable [1].curPoint, 1);
		updatePointsWagon (routeTable [3].curPoint, 3);
		updatePointsWagon (routeTable [29].curPoint, 29);
		updatePointsWagon (routeTable [6].curPoint, 6);
		updatePointsWagon (routeTable [28].curPoint, 28);
		updatePointsWagon (routeTable [9].curPoint, 9);
		updatePointsWagon (routeTable [31].curPoint, 31);
		updatePointsWagon (routeTable [30].curPoint, 30);
		updatePointsWagon (routeTable [33].curPoint, 33);
		updatePointsWagon (routeTable [32].curPoint, 32);
		updatePointsWagon (routeTable [16].curPoint, 16);
		updatePointsWagon (routeTable [19].curPoint, 19);
		updatePointsWagon (routeTable [35].curPoint, 35);
		updatePointsWagon (routeTable [34].curPoint, 34);
		updatePointsWagon (routeTable [22].curPoint, 22);
		updatePointsWagon (routeTable [24].curPoint, 24);
	}
}
