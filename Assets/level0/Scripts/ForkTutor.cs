﻿using UnityEngine;
using System.Collections;

public class ForkTutor : MonoBehaviour {
	private bool isFirstClick;
	private Level1Control test_level_ctrl;
	// Use this for initialization
	void Start () {
		test_level_ctrl = GameObject.Find("gameControl").GetComponent<Level1Control>();
		isFirstClick = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown ()
	{  
		if (isFirstClick) {
				isFirstClick = false;
				test_level_ctrl.updateRouteAndHelpData(1);
		} else {
				isFirstClick = false;
				test_level_ctrl.updateRouteAndHelpData(3);
              
				}
	}

}
