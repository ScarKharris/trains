﻿using UnityEngine;
using System.Collections;

public class Wagon : MonoBehaviour {
	public Camera camera;
	public GameScoreControl gameScoreControl;	// управление очками
	private bool selectWagon; 	//признак задействования вагона для передвижения


	private Vector3 oldScreenPoint;
	private float oldPosY;
	private PointS curPoints;
	private PointS newPoint;	//точка в которую задано перемещение
	private Vector3 intermedPoint; // промежуточная точка, если отсутствует, то равно (-1, -1, -1)
	private float distA=0; // расстояние до соседней точки А
	private float distB=0; // расстояние до соседней точки B
	private BaseControl ctrl;	// ссылка на контрол
	private bool isMoving = false; 	//выполняется ли движение вагона от точки к точке
	private bool isDragging = true;	// можно ли перетаскивать, true - да,  false - нет
	private bool isPointAValid = true;	// точка А - существует?
	private bool isPointBValid = true; // точка B - существует?
	private bool isIntermedPointValid = false; // существует ли промежуточная точка
	private PointerManager pointerManager;	// управление стрелками
	//private PointLightGo pointLightGo;
	private bool directionRotateWagon = false;
    public bool onTgtPnt;	// true - на цалевой точке, в другом случае - false
	// Use this for initialization
	void Start () {

		oldScreenPoint = new Vector3 ();
		onTgtPnt = false;
		ctrl = GameObject.Find("gameControl").GetComponent<BaseControl>();
		pointerManager = GetComponent<PointerManager>();
    //    checkWagonTgt();
	//	pointLightGo = GetComponent<PointLightGo>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//если начато перемещение 
		if (isMoving) {	//плавное перемещение от точки к точке 
			if (isIntermedPointValid)
			{
					transform.position = Vector3.MoveTowards (transform.position, intermedPoint, 1.5f);
				//когда достигнута промежуточная точка
				if (transform.position == intermedPoint)
				{
					rotateWagon();
					isIntermedPointValid = false;
				
				}
			} else {
				transform.position = Vector3.MoveTowards (transform.position, newPoint.curPoint, 1.5f);
             
			//когда достигнута новая точка
				if (transform.position == newPoint.curPoint) {

					//pointLightGo.createPointLightGo();

					isMoving = false;
			
	                //Когда достигается новая точка только тогда меняется текущая 
	           //     Debug.Log("Перемещается!");
					setCurPoints(newPoint);
					rotateWagon();
					checkWagonTgt();
				
			}
			}
		}
	}
	

	 void OnMouseDrag ()
	{  

	}
	void OnMouseUp()
	{
		//уничтожаем стрелки
		
		pointerManager.destroyPointers ();

		if (isMoving)
			return;
		Vector3 curScreenPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, oldScreenPoint.z);
		Vector3 curPosition = camera.ScreenToWorldPoint (new Vector3 (curScreenPoint.x, curScreenPoint.y, oldScreenPoint.z));
		float offsetZ = curPosition.y - oldPosY;
		curPosition.z += offsetZ;
		curPosition.y = oldPosY;
	//	rotateWagon ();

		// подсчет расстояния до точки A	
		float curDistA = Vector3.Distance(curPosition, curPoints.pointA);
		// подсчет расстояния до точки B
		float curDistB = Vector3.Distance (curPosition, curPoints.pointB);
		if(isPointAValid)
		if ((curDistA <= distA * 0.8) || (curPosition.x<curPoints.pointA.x)) {
			// перемещаем вагон в точку A
			isIntermedPointValid = ctrl.setupItermediatePoint(true, ref intermedPoint, curPoints.curPoint);
			if (checkAndMoveWagon (true, true) &&gameScoreControl!=null)
				gameScoreControl.stepsIncrease ();
			directionRotateWagon = false;
				
		}

		if(isPointBValid)
		if ((curDistB <= distB * 0.8)  || (curPosition.x>curPoints.pointB.x)) {
			// перемещаем вагон в точку B
			isIntermedPointValid = ctrl.setupItermediatePoint(false, ref intermedPoint, curPoints.curPoint);
			if(checkAndMoveWagon(false, true) && gameScoreControl!=null)
				gameScoreControl.stepsIncrease ();
			directionRotateWagon = true;
				
		}





	}

	void OnMouseDown()
	{
		//создвние стрелок
		//leftStrelkaClone = (GameObject)Instantiate (leftStrelka,transform.position,new Quaternion());
		//rightStrelkaClone = (GameObject)Instantiate (rightStrelka,transform.position,new Quaternion());
		pointerManager.createPointers();
		//rotateWagon ();
		oldScreenPoint = camera.WorldToScreenPoint(transform.position);
		oldPosY = transform.position.y;

		//установка направления стрелок вагона
		rotatePointerWagon ();

	}

	void OnTriggerEnter(Collider other)
	{
	//	other.GetComponent <>;
	}



	public void setCurPoints(PointS pnts)
	{
		curPoints = pnts;

		validationAndCalcDist ();
	}
	public void setNewPoints(PointS pnts)
	{
		newPoint = pnts;
	}
	public void setIntermediatePoints(PointS pnts)
	{
		newPoint = pnts;
	}
	// расчет расстояния до ближайших точек
	private void validationAndCalcDist()
	{
		if ((curPoints.pointA.x == -1) || (curPoints.pointA.y == -1) || (curPoints.pointA.z == -1))
			isPointAValid = false;
		else {
			isPointAValid = true;
			distA = Vector3.Distance (curPoints.curPoint, curPoints.pointA);
		}

		if ((curPoints.pointB.x == -1) || (curPoints.pointB.y == -1) || (curPoints.pointB.z == -1))
			isPointBValid = false;
		else {
			isPointBValid = true;
			distB = Vector3.Distance (curPoints.curPoint, curPoints.pointB);
		}
	}
	// проверка местоположения вагона, относительно своей целевой точки
	public void checkWagonTgt()
	{
       // Debug.Log(64);
		for (int i = 0; i < ctrl.tgtPoints.Length; i++) {
			// если встали на целевую точку
			if (transform.position == ctrl.tgtPoints [i].position) {
				if (!onTgtPnt) 
					ctrl.enterWagonTgtPnt ();
				onTgtPnt = true;
				return;
			}	

		}
		if (onTgtPnt) {	// если сдвинули с целевой точки
			for (int i = 0; i < ctrl.tgtPoints.Length; i++) {
				if (transform.position != ctrl.tgtPoints [i].position) {
					onTgtPnt = false;
					ctrl.exitWagonTgtPnt ();
					return ;
				}
			}

				
		}
	
			
		
	}

	private void rotateWagon()
	{
	
		//поворот вагона  осуществляется в направлениии точки В или дополнительной точки
		if ((isPointBValid)) {

			Vector3 distance = new Vector3 (0, 0, 0);
			//если движение вправо
			if (directionRotateWagon)
				if (isIntermedPointValid) 
					distance = curPoints.pointB - transform.position;  //если вагон пришел на дополнительную точку (слево направо), то устанавливаем направление на точку В 
				else
					//если вагон в основной точке
					if (ctrl.setupItermediatePoint (false, ref intermedPoint, curPoints.curPoint))
						distance = intermedPoint - transform.position;
					else
						distance = curPoints.pointB - transform.position;
			else
			//если движение влево
				if (isIntermedPointValid)
					distance = transform.position - curPoints.pointA; //если  вагон пришел на дополнительную точку (справо налево), то устанавливаем направление на точку А, но развернув вгаон на 180
				else
					//если вагон в основной точке
					if (ctrl.setupItermediatePoint (false, ref intermedPoint, curPoints.curPoint))
						distance = intermedPoint - transform.position;
					else
						distance = curPoints.pointB - transform.position; 
			Quaternion newRotate = Quaternion.FromToRotation (Vector3.forward, distance);
			transform.rotation = Quaternion.Euler (270, newRotate.eulerAngles.y - 90, 0);
		} 
		

	}
	//установка направления стрелок вагона 
	private void rotatePointerWagon()
	{
		if ((isPointBValid) && (ctrl.checkNearPoint (false, curPoints.pointB, false))) {
			pointerManager.translatePointer (true, curPoints.pointB);
			pointerManager.hideBlockPointer (true);
		} else {

			pointerManager.hidePointer (true);
			pointerManager.translateBlockPointer (true, curPoints.pointB);
		}
		
		if ((isPointAValid) && (ctrl.checkNearPoint (true, curPoints.pointA, false))) {
			pointerManager.translatePointer (false, curPoints.pointA);
			pointerManager.hideBlockPointer (false);

		} else{
			pointerManager.hidePointer (false);
			pointerManager.translateBlockPointer (false, curPoints.pointA);
		}
	}
	public bool checkAndMoveWagon(bool direction, bool move){
		// если точка А  - существует
		// подсчет расстояния до точки A)
		if (direction) {
			if (isPointAValid) {
				//проверям соседнюю точку на наличие вагона и возможность его передвинуть 
				if (ctrl.checkNearPoint (true, curPoints.pointA, move)) {
					if (move)		// флажок перемещения
					{
						isIntermedPointValid = ctrl.setupItermediatePoint(true, ref intermedPoint, curPoints.curPoint);
						ctrl.UpdateWagonPosition (gameObject, curPoints.pointA);
						isMoving = true;
						directionRotateWagon = false;

					}
					return true;
				}
				return false;
			}
		} else {
			// если точка B  - существует
			// подсчет расстояния до точки B
			if (isPointBValid) {
				//проверям соседнюю точку на наличие вагона и возможность его передвинуть 
			
				if (ctrl.checkNearPoint (false, curPoints.pointB, move)) {
					if (move)		// флажок перемещения
					{
						isIntermedPointValid = ctrl.setupItermediatePoint(false, ref intermedPoint, curPoints.curPoint);
						ctrl.UpdateWagonPosition (gameObject, curPoints.pointB);
						directionRotateWagon = true;
						isMoving = true;

					}
					return true;
				}
				return false;
			}
		}
		return false;
		
	}
}

