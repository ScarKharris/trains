﻿using UnityEngine;
using System.Collections;

public class clickGrab : MonoBehaviour {
	private TutorAnimEvent m_tae;	// чтобы сообщить tutorAnimEvent о событии
	private bool isClicked;	// был факт нажатия на объект?
	// Use this for initialization
	void Start () {
		m_tae = Camera.main.GetComponent<TutorAnimEvent>();
		isClicked = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown ()
	{
		if (!isClicked)
			m_tae.on_tutorPressHoldWagon();	// вызываем событие
		isClicked = true;
	}
}
