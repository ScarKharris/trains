﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameScoreControl : MonoBehaviour {

	public int stepsCount = 0;
	public int countStars;
	public Text gameStepsCountText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void stepsIncrease()
	{
		stepsCount++;
		gameStepsCountText.text = stepsCount.ToString();
	}
	public void saveScore(){
       
		int oldStepsCount = 0;
		oldStepsCount = PlayerPrefs.GetInt (Application.loadedLevelName+ "_stepsCount");
		//if(oldStepsCount>stepsCount)
		PlayerPrefs.SetInt(Application.loadedLevelName+"_stepsCount", stepsCount);
	}
}
