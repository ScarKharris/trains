﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkRound : Fork {

	private byte m_nextIndex;	// номер следующего состояния, в которое попадет форк
	private byte m_prevIndex; // предыдущего
    private bool fl_changeFork;
   
	// Use this for initialization
	void Start () {
        fl_changeFork = true;
		m_nextIndex = 1;
		m_prevIndex = 0;
		ctrl = GameObject.Find("gameControl").GetComponent<BaseControl>();
	}

	void OnMouseDown()
	{
        anim_switchFork.SetBool("switch_on", fl_changeFork);
        anim_switchLightWallFork.SetBool("switchLightWallFork", fl_changeFork);
        fl_changeFork = !fl_changeFork;

		ctrl.switchFork (switchIndex);
		findChangePositionWagon();
		forkRails[m_nextIndex].SetActive(true);
		forkRails[m_prevIndex].SetActive(false);

		m_nextIndex++;
		m_prevIndex++;
		if (m_nextIndex == forkRails.Length) 
			m_nextIndex = 0;
		if (m_prevIndex == forkRails.Length)
			m_prevIndex = 0;

	}

	override public void findChangePositionWagon()
	{

		GameObject[] wagonsAll = GameObject.FindGameObjectsWithTag ("wagon");
		for (int i = 0; i<wagonsAll.Length; i++) {
				
					changePositionWagon(wagonsAll[i],forkRails[m_prevIndex], forkRails[m_nextIndex]);
		}

	}
}
