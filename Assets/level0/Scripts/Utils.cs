﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public struct PointS{
	public Vector3 curPoint;
	public Vector3 pointA;
	public Vector3 pointB;

	public PointS(Vector3 cP, Vector3 pA, Vector3 pB){
		curPoint = cP;
		pointA = pA;
		pointB = pB;
	}

}
[System.Serializable]
public struct TransformS{
	public Transform curPoint;
	public Transform pointA;
	public Transform pointB;
	
	public TransformS(Transform cP, Transform pA, Transform pB){
		curPoint = cP;
		pointA = pA;
		pointB = pB;
	}
	
}
public static class Const
{
	static public readonly Vector3 NULLp = new Vector3(-1, -1, -1);
}
