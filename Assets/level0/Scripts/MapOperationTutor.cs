﻿using UnityEngine;
using System.Collections;

public class MapOperationTutor : MonoBehaviour {
    public Camera camera;
	// ограничители камеры при движении
	public float zd;		// нижний ограничитель по Z
	public float zu;	// верхний ограничитель по Z
	public float xl;	// левый ограничитель по X
	public float xr;  // правый ограничитель по X
	// ограничители камеры при масштабировании
	private float maxScale = 5.0f;
	private float minScale = 65.0f;
    private float koefSpeed; // коэффициенты скорости прокрутки
    private float koefScale; // коэффициент масштабирования
    private float oldDist = 0;	// старое значение расстояния между пальцами при масштабировании
	private Transform newCamTransform; // для проверки выхода камеры за границу карты при прокрутке
	private Vector3 newCamPos; // для проверки выхода камеры за границу карты при прокрутке и масштабировании
    private float oldY;	// ось Y не меняется, потому сохраняем старое значение
    private bool fl_zoom = false;
    
    Vector3 currentPositon, deltaPositon, lastPositon;
    // Use this for initialization
    void Start()
    {
        koefSpeed = 0.035f;
        koefScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {

        /*	if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Moved) {
                Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;
                camera.transform.Translate(-touchDeltaPosition.x * 0.10f, -touchDeltaPosition.y * 0.10f, 0);
            }*/
        if ((Input.GetAxis("Mouse ScrollWheel") != 0))
        {
            fl_zoom = true;
         //    if (camera.GetComponent<TutorAnimEvent>().fl_TutorZoom)
         //       camera.GetComponent<Animator>().enabled = false;
            float zoom = Input.GetAxis("Mouse ScrollWheel");
            if (zoom != 0)
			{
			   newCamPos = Input.GetAxis("Mouse ScrollWheel") * 10 * koefScale * camera.transform.forward + camera.transform.position;
				if (newCamPos.y < maxScale)
					newCamPos.y = maxScale;
				if (newCamPos.y > minScale)
					newCamPos.y = minScale;
				camera.transform.position = newCamPos;

			}
        }

        if (Input.touchCount == 2)
        {
        //    if (camera.GetComponent<TutorAnimEvent>().fl_TutorZoom)
        //        camera.GetComponent<Animator>().enabled = false;
            fl_zoom = true;
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            if ((touch1.phase == TouchPhase.Moved) || (touch2.phase == TouchPhase.Moved))
            {
				float newDist = Vector2.Distance(touch1.position, touch2.position);
				if (newDist > oldDist)
					newCamPos = koefScale * camera.transform.forward + camera.transform.position;		// приближаем камеру
				if (newDist < oldDist)
					newCamPos = -koefScale * camera.transform.forward + camera.transform.position;		// удаляем камеру
				if (newCamPos.y < maxScale)	// проверки, не вышли ли за границы
					newCamPos.y = maxScale;
				if (newCamPos.y > minScale)
					newCamPos.y = minScale;
				camera.transform.position = newCamPos;

                oldDist = newDist;
            }

        }
        if (fl_zoom && Input.touchCount == 1)
        {
        //    if (fl_zoom && camera.GetComponent<TutorAnimEvent>().fl_TutorZoom)
            {
       //         camera.GetComponent<Animator>().enabled = true;
       //         camera.GetComponent<TutorAnimEvent>().on_nextButton_tutorZoomMap();
            }
            lastPositon = Input.mousePosition;
            fl_zoom = false;
          
        }
    }
    void OnMouseDrag()
    {
        if (!fl_zoom && Input.touchCount != 2)
        {
          
            currentPositon = Input.mousePosition;
            deltaPositon = currentPositon - lastPositon;
            lastPositon = currentPositon;
			// проверяем, не вышли ли за рамки при прокрутке 
			newCamTransform = camera.transform;
			newCamTransform.Translate(-deltaPositon.x * koefSpeed, -deltaPositon.y * koefSpeed, 0);
			newCamPos = newCamTransform.position;
			if (newCamPos.x < xl)	// проверки
				newCamPos.x = xl;
			if (newCamPos.x > xr)	// проверки
				newCamPos.x = xr;
			if (newCamPos.z < zd)	// проверки
				newCamPos.z = zd;
			if (newCamPos.z > zu)	// проверки
				newCamPos.z = zu;
			camera.transform.position = newCamPos;
           
        }
      
    }
    void OnMouseUp()
    {      
       /* if (fl_zoom&&camera.GetComponent<TutorAnimEvent>().fl_TutorZoom)
        {
            camera.GetComponent<Animator>().enabled = true;
            camera.GetComponent<TutorAnimEvent>().on_nextButton_tutorZoomMap();
            fl_zoom = false;
        }
       
        if (camera.GetComponent<TutorAnimEvent>().fl_TutorMove)
        {
            camera.GetComponent<Animator>().enabled = true;
            camera.GetComponent<TutorAnimEvent>().on_nextButton_tutorMoveMap();
        }
      */

    }
    void OnMouseDown()
    { 
        if (!fl_zoom && Input.touchCount != 2)
        {
            lastPositon = Input.mousePosition;
        }
      
     //   if (camera.GetComponent<TutorAnimEvent>().fl_TutorMove)
     //       camera.GetComponent<Animator>().enabled = false;
        //oldWorldPoint = camera.ScreenToWorldPoint(new Vector3 (Input.mousePosition.x, 0, Input.mousePosition.y));
        //oldY = camera.transform.position.y;
    }
    /*	void OnMouseDrag()
        {
            Vector3 newWorldPoint = camera.ScreenToWorldPoint(new Vector3 (Input.mousePosition.x, 0, Input.mousePosition.y));
            Vector3 offset = newWorldPoint - oldWorldPoint;
            Debug.Log (offset);
            offset.Set(offset.x * koefSpeed, offset.y * koefSpeed, offset.z * koefSpeed);
            Vector3 oldPos = camera.transform.position;
            Vector3 translate = new Vector3(oldPos.x - offset.x, 0, oldPos.z - offset.z);
            if (translate.x < xl)	// проверки
                translate.x = xl;
            if (translate.x > xr)	// проверки
                translate.x = xr;
            if (translate.z < zd)	// проверки
                translate.z = zd;
            if (translate.z > zu)	// проверки
                translate.z = zu;
            camera.transform.position = new Vector3(translate.x, oldY, translate.z);
        }*/
}
