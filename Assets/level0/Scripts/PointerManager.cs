﻿using UnityEngine;
using System.Collections;

public class PointerManager : MonoBehaviour {
	public GameObject leftPointer;		//внешняя ссылка на префаб
	private GameObject leftPointerClone; //внутренняя ссылка на префаб
	public GameObject rightPointer;
	private GameObject rightPointerClone;
	public GameObject rightBlockPointer;
	private GameObject rightBlockPointerClone;
	public GameObject leftBlockPointer;
	private GameObject leftBlockPointerClone;
    private GameObject cam;
    private int AdvAngleCam = 0;
	private readonly Vector3 NULLp = new Vector3(-1, -1, -1);
	//private railCoupler1
	// Use this for initialization
	void Start () {
		//ctrl = GameObject.Find("gameControl").GetComponent<MainControl>();
       
        cam = GameObject.Find("Camera");
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	// отображение стрелок
	public void createPointers()
	{
		leftPointerClone = (GameObject)Instantiate (leftPointer,transform.position,new Quaternion());
		rightPointerClone = (GameObject)Instantiate (rightPointer,transform.position,new Quaternion());

		leftBlockPointerClone = (GameObject)Instantiate (leftBlockPointer,transform.position,new Quaternion());
		rightBlockPointerClone = (GameObject)Instantiate (rightBlockPointer,transform.position,new Quaternion());
        //Debug.Log(cam.transform.rotation.eulerAngles.x);
        if (cam.transform.rotation.eulerAngles.y == 270)
        {
           
            leftPointerClone.transform.FindChild("left").gameObject.transform.rotation = Quaternion.Euler(75, -90, 0);
            rightPointerClone.transform.FindChild("right").gameObject.transform.rotation = Quaternion.Euler(75, -90, 0);
            leftBlockPointerClone.transform.FindChild("left").gameObject.transform.rotation = Quaternion.Euler(75, -90, 0);
            rightBlockPointerClone.transform.FindChild("right").gameObject.transform.rotation = Quaternion.Euler(75, -90, 0);
        }

	}
	// скрыть стрелки
	public void destroyPointers()
	{
		Destroy (leftPointerClone);
		Destroy (rightPointerClone);
		Destroy (leftBlockPointerClone);
		Destroy (rightBlockPointerClone);
	}
	// перемещение стрелки (true - правая, false - левая)
	public void translatePointer(bool direct, Vector3 posPoint)
	{

       

		GameObject pointer;// = new GameObject ();
		Vector3	distance = posPoint - transform.position; 
		Quaternion newRotate = Quaternion.FromToRotation (Vector3.forward, distance);

      
		if (direct) {
			pointer = rightPointerClone;
			pointer.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x -270, newRotate.eulerAngles.y-90, transform.rotation.eulerAngles.z);

            if (newRotate.eulerAngles.y > 170)
            {
                rightPointerClone.transform.FindChild("right").gameObject.transform.rotation = Quaternion.Euler(60, 0, 0);
            }
           
            
		} else {
			pointer = leftPointerClone;
         
			pointer.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x -270, newRotate.eulerAngles.y-270, transform.rotation.eulerAngles.z);
          //  Debug.Log(newRotate.eulerAngles.y);
            if (newRotate.eulerAngles.y < 170 || newRotate.eulerAngles.y>350)
            {
                leftPointerClone.transform.FindChild("left").gameObject.transform.rotation = Quaternion.Euler(60, 0, transform.rotation.eulerAngles.z);
            }
           
          
		}
		if (pointer)
		{
				//поворот правой стрелки в направлении точки В 
				//pointer.transform.rotation = transform.rotation;
				//pointer.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x - 270, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
				//перемещение стрелки за вагоном 
			pointer.transform.position = transform.position;	
			pointer.SetActive(true);
          
		}

	}
	public void translateBlockPointer(bool direct, Vector3 posPoint)
	{
		GameObject pointer;// = new GameObject ();
		Vector3	distance = posPoint - transform.position; 
		Quaternion newRotate = Quaternion.FromToRotation (Vector3.forward, distance);
      
		if (direct) {
			pointer = rightBlockPointerClone;
           

			if(posPoint != NULLp)
				    pointer.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x -270, newRotate.eulerAngles.y-90, transform.rotation.eulerAngles.z);
            if (newRotate.eulerAngles.y > 170)
            {
                rightPointerClone.transform.FindChild("right").gameObject.transform.rotation = Quaternion.Euler(60, 0, 0);
            }

           
		} else {

			pointer = leftBlockPointerClone;
			if(posPoint != NULLp)
               pointer.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x -270, newRotate.eulerAngles.y-270, transform.rotation.eulerAngles.z);

            if (newRotate.eulerAngles.y < 170 || newRotate.eulerAngles.y > 350)
            {
                leftPointerClone.transform.FindChild("left").gameObject.transform.rotation = Quaternion.Euler(60, 0, transform.rotation.eulerAngles.z);
            }
           
		}
		if (pointer)
		{
			//поворот правой стрелки в направлении точки В 
			//pointer.transform.rotation = transform.rotation;
			//pointer.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x - 270, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
			//перемещение стрелки за вагоном 
			pointer.transform.position = transform.position;	
			pointer.SetActive(true);
           
		}
	}

	public void hidePointer(bool direct)
	{
		GameObject pointer;
		if (direct) {
			pointer = rightPointerClone;
		} else {
			pointer = leftPointerClone;
		}
		if (pointer) {
			pointer.SetActive (false);
		}
	
	}
	public void hideBlockPointer(bool direct)
	{
		GameObject blockPointer;
		if (direct) {
			blockPointer = rightBlockPointerClone; 
		
		} else {
			blockPointer = leftBlockPointerClone; 

		}

		if (blockPointer) {
			blockPointer.SetActive (false);
		}
	}
}