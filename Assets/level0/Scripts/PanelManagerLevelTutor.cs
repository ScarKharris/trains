﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;


public class PanelManagerLevelTutor : MonoBehaviour {
	
	public GameObject panelDialogExitMenu;
	public GameObject panelDialogAgainLevel;
	public GameObject panelLevelCompletePre;
	public GameObject panelLevelCompleteFinal;
	public Text countStepsText;
	public Image star1Image;
	public Image star2Image;
	public Image star3Image;
	public GameObject photo;
	public Animator photoButtonAnimator;
	public GameScoreControl gameScoreControl;	// управление очками
	private TutorAnimEvent m_tae;
	private GameObject m_current_panel;
   

	void Start () {
       
		m_tae = GameObject.Find("Camera").GetComponent<TutorAnimEvent>();
	}

	public void openMenu ()
	{
		panelDialogExitMenu.SetActive (true);
		m_current_panel = m_tae.current_active_panel;
		m_current_panel.SetActive (false);
	}
	public void on_button_exitMenuYes ()
	{
		
		Application.LoadLevel ("MainMenu");
	}
	public void on_button_exitMenuNo ()
	{
		panelDialogExitMenu.SetActive (false);
		m_current_panel.SetActive (true);
	}
	public void openDialogAgainLevel ()
	{
		panelDialogAgainLevel.SetActive (true);
		m_current_panel = m_tae.current_active_panel;
		m_current_panel.SetActive (false);
	}
	public void on_button_againLevelYes ()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}
	public void on_button_nextLevel()
	{
       // string numberLevelstr = Application.loadedLevelName.Remove(0, 5);
        
        //int numberNextLevel = SceneManager.GetActiveScene().buildIndex;
     //   Debug.Log(numberNextLevel);
      //  Application.LoadLevel(numberNextLevel);
	}
	public void on_button_photo()
	{
		if (photoButtonAnimator.enabled)
			photoButtonAnimator.SetTrigger ("endPBF");
		
		if (photo.active) {
			//	photo.SetActive (false);
			photo.GetComponent<Animator> ().SetTrigger ("ShowHide");
		} else {
			photo.SetActive (true);
			photo.GetComponent<Animator> ().SetTrigger ("ShowHide");
		}
	}
	public void on_button_againLevelNo ()
	{
		panelDialogAgainLevel.SetActive (false);
		m_current_panel.SetActive (true);
	}
	public void on_button_exitMenu ()
	{
		Application.LoadLevel ("MainMenu");
	}
	
	public void showPanelLevelCompletePre(){
		
		panelLevelCompletePre.SetActive (true);
		
	}
	public void on_panelLevelCompletePre(){
		
		panelLevelCompletePre.SetActive (false);
		panelLevelCompleteFinal.SetActive (true);
		showPanelLevelCompleteFinal();
		
	}
	public void showPanelLevelCompleteFinal(){
		
		int countStepStar1=0, countStepStar2=0, countStepStar3=0;
		
		if (Application.loadedLevel == 1) {
			countStepStar3 = Level1_starsData.countStepStar3;
			countStepStar2 = Level1_starsData.countStepStar2;
			countStepStar1 = Level1_starsData.countStepStar1;
		}
		if (Application.loadedLevel == 2) {
			countStepStar3 = Level2_starsData.countStepStar3;
			countStepStar2 = Level2_starsData.countStepStar2;
			countStepStar1 = Level2_starsData.countStepStar1;
		}
		
		//вывод панели пройденного уровня, вывод очков и подсчет звезд
		panelLevelCompletePre.SetActive (false);
		
		countStepsText.text =  gameScoreControl.stepsCount.ToString();
		Color color_noActive = new Color();
		color_noActive.r = 0.05f;
		color_noActive.g = 0.05f;
		color_noActive.b = 0.2f;
		color_noActive.a = 1f;
		Color color_active = new Color();
		color_active.r = 1f;
		color_active.g = 1f;
		color_active.b = 1f;
		color_active.a = 1f;
		
		if (countStepStar3 >= gameScoreControl.stepsCount) {
			star1Image.color = color_active;
			star2Image.color = color_active;
			star3Image.color = color_active;
		} else if (countStepStar2 >= gameScoreControl.stepsCount) {
			star1Image.color = color_active;
			star2Image.color = color_active;
			star3Image.color = color_noActive;
		} else if (countStepStar1 >= gameScoreControl.stepsCount) {
			star1Image.color = color_active;
			star2Image.color = color_noActive;
			star3Image.color = color_noActive;
		} else {
			star1Image.color = color_noActive;
			star2Image.color = color_noActive;
			star1Image.color = color_noActive;
		}
		panelLevelCompleteFinal.SetActive (true);
		
		//сохранение количества ходов, также нужно для отображения в главном меню
		gameScoreControl.saveScore ();
		
	}
	
	
}
