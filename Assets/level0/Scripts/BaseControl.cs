using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseControl : MonoBehaviour {
	public TransformS[] points;
	public TransformS[] intermedPoints;
	public GameObject[] wagons;
	public PanelManagerLevel panelManagerLevel;
	public Animator anim_loko;
	public Animator anim_Camera;
	public GameObject obj_loko;

	public Material matGreen;
	public Material matRed;
	public Material matOff;
	public Material matBackGreen;
	public Material matBackRed;
	protected PointS[] routeTable;
	protected PointS[] intermedPointsTable; // таблица промежуточных точек
	public Transform[] tgtPoints; // массив целевых точек, т.е. точки, на которые надо поставить вагоны
	private bool fl_levelComplete = false;
	//private readonly Vector3 NULLp = new Vector3(-1, -1, -1);
	protected byte countTgtPnt;	// количество достигнутых целевых точек
	protected bool[] checkedForks;
	private readonly Vector3 baseScale = new Vector3(1, -1, 1);
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	protected void initTables()
	{
		//	Application.targetFrameRate = 30;
		// составляем таблицу
		routeTable = new PointS[points.Length];
		for (int i =0; i<points.Length; i++) {
			routeTable[i].curPoint = points[i].curPoint.position;
			if(points [i].pointA == null)
				routeTable[i].pointA = Const.NULLp;
			else
				routeTable[i].pointA = points[i].pointA.position;
			
			if(points [i].pointB == null)
				routeTable[i].pointB = Const.NULLp;
			else
				routeTable[i].pointB = points[i].pointB.position;
		}
		
		// заполняем таблицу промежуточных точек
		// нужно переписать маршрут с учетом новых (промежуточных точек)
		intermedPointsTable = new PointS[intermedPoints.Length];
		for (int i =0; i<intermedPoints.Length; i++) {
			intermedPointsTable [i].curPoint = intermedPoints[i].curPoint.position;
			if(intermedPoints [i].pointA == null)
				intermedPointsTable[i].pointA = Const.NULLp;
			else
				intermedPointsTable[i].pointA = intermedPoints[i].pointA.position;
			
			if(intermedPoints[i].pointB == null)
				intermedPointsTable [i].pointB = Const.NULLp;
			else
				intermedPointsTable [i].pointB = intermedPoints[i].pointB.position;
		}
	}

	public PointS findPointSByVector3(Vector3 point)
	{
		PointS pointS;
		pointS.curPoint = Const.NULLp;
		pointS.pointA = Const.NULLp;
		pointS.pointB = Const.NULLp;
		for (int i = 0; i<routeTable.Length; i++) {
			if (routeTable[i].curPoint == point)
				pointS = routeTable[i];	
		}
		return pointS;

	}
	// вызывается, когда вагон меняет свое местоположение
	public void UpdateWagonPosition(GameObject wagon, Vector3 newPoint)
	{
		foreach(PointS point in routeTable)
		{
			if (point.curPoint == newPoint)
			{
				// устанавливаем новую точку
				wagon.GetComponent<Wagon>().setNewPoints (point);
				// устанавливаем промежуточную точку, если есть

			}
		}
	}

	//ообновление информации о соседних точках у вагона
	//первый параметр - точка на которой стоит вагон, второй - индеккс этой(или любой другой) точки в массиве точек
	protected void updatePointsWagon(Vector3 curpoint, int indPointArr)
	{
		for (int i = 0; i < wagons.Length; i++) {
			if (curpoint == wagons[i].transform.position)
				wagons[i].GetComponent<Wagon>().setCurPoints (routeTable[indPointArr]);

		}
	}

	virtual public void enterWagonTgtPnt()	// ва
	{
		Debug.Log ("Встал на целевую точку!");
		countTgtPnt++;
		if (countTgtPnt == wagons.Length) {
			
			//привязка вагонов к поезду(вагоны будут дочерними обьектами поезда), так как анимация только у поезда
			for (int i = 0; i < wagons.Length; i++)
			{
				wagons [i].transform.parent  = obj_loko.transform;
				wagons [i].transform.localScale  = baseScale;
			}
			//включение анимации камеры
			anim_Camera.GetComponent<Animator> ().enabled = true;
			//включение анимации поезда
			anim_loko.SetBool ("start", true);
			//показ текста  
			panelManagerLevel.showPanelLevelCompletePre ();
			
			Debug.Log ("Все вагоны расставлены!");
		}
	}

	virtual public void exitWagonTgtPnt()	// ва
	{
		Debug.Log ("Покинул целевую точку!");
		countTgtPnt--;
	}
	// проверяет , есть ли возможность перемещения вагона в заданном направлении (A или B)	
	//	Если вагон переетить не удалость, то возвращет false и позиции не меняются
	//	первый параметр означает направление true - влево, false - вправо
	public bool checkNearPoint(bool direction, Vector3 point, bool move)
	{
		//поиск вагона находящегося на точке
		foreach (GameObject wagon in wagons) {
			if(wagon.transform.position == point)
			{
				
				//Если вагон найден, то передвигаем его в напрвлении  direction
				if(wagon.GetComponent<Wagon>().checkAndMoveWagon(direction, move))
					return true; //Если вагон передвинут
				else
					return false;
			}
		}
		//Если на точке вагона нет
		return true;
	}
	// возвращает true, если существует промежуточная точка, false- если отсутствует
	public bool setupItermediatePoint(bool direction, ref Vector3 intermedPoint, Vector3 curPosition)
	{
		bool isFound = false;
		// если промежуточной точки не существует - false
		if (intermedPointsTable != null)
		foreach (PointS point in intermedPointsTable)
		{
			if (point.curPoint == curPosition)
			{
				Vector3 tmpPoint;
				if (direction)
					tmpPoint = point.pointA;
				else
					tmpPoint = point.pointB;
						
					if (tmpPoint != Const.NULLp)
					{
						intermedPoint = tmpPoint;
						isFound = true;
					} 
			}
		}

		return isFound;
	}
	virtual public void switchFork(byte switchForkIndex)
	{

	}
	// блок работы с переключателями форк
	/*private void enableForkSwitch1()
	{
		
		routeTable [3].pointB = routeTable[2].curPoint;
		routeTable [1].pointA = routeTable [2].curPoint;
		routeTable [10].pointB = routeTable [9].curPoint;
		routeTable [8].pointA = routeTable [9].curPoint;

		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[1].curPoint,1);
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[8].curPoint,8);

//		GameObject.Find("TrafficLight 5/Sphere Green").GetComponent<Renderer>().material = matGreen;
//		GameObject.Find("TrafficLight 5/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 5/Sphere Green/Point light").SetActive (true);
//		GameObject.Find("TrafficLight 5/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 5/Sphere Red/Point light").SetActive (false);

	//	GameObject.Find("TrafficLight 4/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 4/Sphere Green/Point light").SetActive (false);
	//	GameObject.Find("TrafficLight 4/Sphere Red").GetComponent<Renderer>().material  = matRed;
	//	GameObject.Find("TrafficLight 4/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 4/Sphere Red/Point light").SetActive (true);

	//	GameObject.Find("TrafficLight 3/Sphere Green").GetComponent<Renderer>().material  = matGreen;
	//	GameObject.Find("TrafficLight 3/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 3/Sphere Green/Point light").SetActive (true);
	//	GameObject.Find("TrafficLight 3/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 3/Sphere Red/Point light").SetActive (false);
	}

	private void disableForkSwitch1()
	{
		routeTable [3].pointB = Const.NULLp;
		routeTable [1].pointA = routeTable [20].curPoint;
		routeTable [10].pointB = routeTable [20].curPoint;
		routeTable [8].pointA = routeTable [21].curPoint;


		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[1].curPoint,1);
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[8].curPoint,8);

//		GameObject.Find("TrafficLight 5/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 5/Sphere Green/Point light").SetActive (false);
//		GameObject.Find("TrafficLight 5/Sphere Red").GetComponent<Renderer>().material  = matRed;
//		GameObject.Find("TrafficLight 5/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 5/Sphere Red/Point light").SetActive (true);
		
	//	GameObject.Find("TrafficLight 4/Sphere Green").GetComponent<Renderer>().material  = matGreen;
	//	GameObject.Find("TrafficLight 4/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 4/Sphere Green/Point light").SetActive (true);
	//	GameObject.Find("TrafficLight 4/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 4/Sphere Red/Point light").SetActive (false);
		
	//	GameObject.Find("TrafficLight 3/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 3/Sphere Green/Point light").SetActive (false);
	//	GameObject.Find("TrafficLight 3/Sphere Red").GetComponent<Renderer>().material  = matRed;
	//	GameObject.Find("TrafficLight 3/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 3/Sphere Red/Point light").SetActive (true);
	}

	private void enableForkSwitch2()
	{	
		routeTable [10].pointA = Const.NULLp;
		routeTable [16].pointB = routeTable [15].curPoint;
		routeTable [14].pointA = routeTable [15].curPoint;
		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[16].curPoint,16);
		updatePointsWagon(routeTable[14].curPoint,14);
	
	//	GameObject.Find("TrafficLight 2/Sphere Green").GetComponent<Renderer>().material  = matGreen;
	//	GameObject.Find("TrafficLight 2/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 2/Sphere Green/Point light").SetActive (true);
	//	GameObject.Find("TrafficLight 2/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 2/Sphere Red/Point light").SetActive (false);
		
	//	GameObject.Find("TrafficLight 1/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 1/Sphere Green/Point light").SetActive (false);
	//	GameObject.Find("TrafficLight 1/Sphere Red").GetComponent<Renderer>().material  = matRed;
	//	GameObject.Find("TrafficLight 1/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 1/Sphere Red/Point light").SetActive (true);
	}

	private void disableForkSwitch2()
	{
		routeTable [10].pointA = routeTable [19].curPoint;
		routeTable [16].pointB = routeTable [19].curPoint;
		routeTable [14].pointA = Const.NULLp;

		updatePointsWagon(routeTable[10].curPoint,10);
		updatePointsWagon(routeTable[16].curPoint,16);
		updatePointsWagon(routeTable[14].curPoint,14);

	//	GameObject.Find("TrafficLight 2/Sphere Green").GetComponent<Renderer>().material  = matOff;

//		GameObject.Find ("TrafficLight 2/Sphere Green/Point light").SetActive (false);
	//	GameObject.Find("TrafficLight 2/Sphere Red").GetComponent<Renderer>().material  = matRed;
	//	GameObject.Find("TrafficLight 2/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 2/Sphere Red/Point light").SetActive (true);
		
	//	GameObject.Find("TrafficLight 1/Sphere Green").GetComponent<Renderer>().material  = matGreen;
	//	GameObject.Find("TrafficLight 1/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 1/Sphere Green/Point light").SetActive (true);
	//	GameObject.Find("TrafficLight 1/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 1/Sphere Red/Point light").SetActive (false);



	}	*/
}