using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PanelManagerLevel : MonoBehaviour {

	public GameObject panelDialogExitMenu;
	public GameObject panelDialogAgainLevel;
	public GameObject panelLevelCompletePre;
	public GameObject panelLevelCompleteFinal;
	public Text countStepsText;
    public GameObject photo;
	public Image star1Image;
	public Image star2Image;
	public Image star3Image;
    public Image starEnableImage;
    public Image starDisableImage;
    public Animator photoButtonAnimator;
	public GameScoreControl gameScoreControl;	// управление очками

    public void on_button_photo()
    {
        if (photoButtonAnimator.enabled)
            photoButtonAnimator.SetTrigger("endPBF");

        if (photo.active)
        {
            //	photo.SetActive (false);
            photo.GetComponent<Animator>().SetTrigger("ShowHide");
        }
        else
        {
            photo.SetActive(true);
            photo.GetComponent<Animator>().SetTrigger("ShowHide");
        }
    }
	public void openMenu ()
	{
		panelDialogExitMenu.SetActive (true);
	}
	public void on_button_exitMenuYes ()
	{

		Application.LoadLevel ("MainMenu");
	}
	public void on_button_exitMenuNo ()
	{
		panelDialogExitMenu.SetActive (false);
	}
	public void openDialogAgainLevel ()
	{
		panelDialogAgainLevel.SetActive (true);
	}
	public void on_button_againLevelYes ()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}
    public void on_button_nextLevel()
    {
        int numberNextLevel = SceneManager.GetActiveScene().buildIndex+1;
        Application.LoadLevel(numberNextLevel);
    }

	public void on_button_againLevelNo ()
	{
		panelDialogAgainLevel.SetActive (false);
	}
	public void on_button_exitMenu ()
	{
		Application.LoadLevel ("MainMenu");
	}

	public void showPanelLevelCompletePre(){

		panelLevelCompletePre.SetActive (true);

	}
	public void on_panelLevelCompletePre(){

		panelLevelCompletePre.SetActive (false);
		panelLevelCompleteFinal.SetActive (true);
        showPanelLevelCompleteFinal();

	}
	public void showPanelLevelCompleteFinal(){
		 
		int countStepStar1=0, countStepStar2=0, countStepStar3=0;
        int numberLevel = SceneManager.GetActiveScene().buildIndex;
        if (numberLevel == 1)
        {
			countStepStar3 = Level1_starsData.countStepStar3;
			countStepStar2 = Level1_starsData.countStepStar2;
			countStepStar1 = Level1_starsData.countStepStar1;
		}
        if (numberLevel == 2)
        {
			countStepStar3 = Level2_starsData.countStepStar3;
			countStepStar2 = Level2_starsData.countStepStar2;
			countStepStar1 = Level2_starsData.countStepStar1;
		}
        if (numberLevel == 3)
        {
            countStepStar3 = Level3_starsData.countStepStar3;
            countStepStar2 = Level3_starsData.countStepStar2;
            countStepStar1 = Level3_starsData.countStepStar1;
        }
        if (numberLevel == 4)
        {
            countStepStar3 = Level4_starsData.countStepStar3;
            countStepStar2 = Level4_starsData.countStepStar2;
            countStepStar1 = Level4_starsData.countStepStar1;
        }
        if (numberLevel == 5)
        {
            countStepStar3 = Level5_starsData.countStepStar3;
            countStepStar2 = Level5_starsData.countStepStar2;
            countStepStar1 = Level5_starsData.countStepStar1;
        }
        if (numberLevel == 6)
        {
            countStepStar3 = Level6_starsData.countStepStar3;
            countStepStar2 = Level6_starsData.countStepStar2;
            countStepStar1 = Level6_starsData.countStepStar1;
        }
        if (numberLevel == 7)
        {
            countStepStar3 = Level7_starsData.countStepStar3;
            countStepStar2 = Level7_starsData.countStepStar2;
            countStepStar1 = Level7_starsData.countStepStar1;
        }

		//вывод панели пройденного уровня, вывод очков и подсчет звезд
		panelLevelCompletePre.SetActive (false);

		countStepsText.text =  gameScoreControl.stepsCount.ToString();
		Color color_noActive = new Color();
		color_noActive.r = 0.05f;
		color_noActive.g = 0.05f;
		color_noActive.b = 0.2f;
		color_noActive.a = 1f;
		Color color_active = new Color();
		color_active.r = 1f;
		color_active.g = 1f;
		color_active.b = 1f;
		color_active.a = 1f;
       
		if (countStepStar3 >= gameScoreControl.stepsCount) {
            star1Image.sprite = starEnableImage.sprite;
            star2Image.sprite = starEnableImage.sprite;
            star3Image.sprite = starEnableImage.sprite;
		} else if (countStepStar2 >= gameScoreControl.stepsCount) {
            star1Image.sprite = starEnableImage.sprite;
            star2Image.sprite = starEnableImage.sprite;
            star3Image.sprite = starDisableImage.sprite; ;
		} else if (countStepStar1 >= gameScoreControl.stepsCount) {
            star1Image.sprite = starEnableImage.sprite;
            star2Image.sprite = starDisableImage.sprite;
            star3Image.sprite = starDisableImage.sprite; ;
		} else {
            star1Image.sprite = starDisableImage.sprite; ;
            star2Image.sprite = starDisableImage.sprite; ;
            star1Image.sprite = starDisableImage.sprite; ;
		}
		panelLevelCompleteFinal.SetActive (true);
       
		//сохранение количества ходов, также нужно для отображения в главном меню
		gameScoreControl.saveScore ();

	}


}
