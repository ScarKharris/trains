﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level8Control : BaseControl {
	private byte checkedForksRound1;
	private byte checkedForksRound2;
	private bool isChecked = true;
	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected void init()
	{
		initTables ();
		wagons[0].transform.position = routeTable[0].curPoint;
		wagons[0].GetComponent<Wagon>().setCurPoints (routeTable[0]);
		wagons[1].transform.position = routeTable[11].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[11]);
		wagons[2].transform.position = routeTable[2].curPoint;
		wagons[2].GetComponent<Wagon>().setCurPoints (routeTable[2]);
		wagons [1].transform.rotation = Quaternion.Euler (270, 3f, 0);
		wagons [2].transform.rotation = Quaternion.Euler (270, 90f, 0);
		checkedForks = new bool[3];
		checkedForks [0] = true;
		checkedForks [1] = true;
		checkedForks [2] = true;
	}

	override public void switchFork(byte switchForkIndex)
	{	
		if (switchForkIndex == 0)
			switch (checkedForksRound1) 
			{
			case 0:
				forkSwitch11();
				checkedForksRound1 = 1;
				break;
			case 1:
				forkSwitch12();
				checkedForksRound1 = 2;
				break;
			case 2:
				forkSwitch13();
				checkedForksRound1 = 3;
				break;
			case 3:
				forkSwitch14();
				checkedForksRound1 = 0;
				break;
			}

		if (switchForkIndex == 1)
			switch (checkedForksRound2) 
		{
			case 0:
			forkSwitch21();
			checkedForksRound2 = 1;
			break;
			case 1:
			forkSwitch22();
			checkedForksRound2 = 2;
			break;
			case 2:
			forkSwitch23();
			checkedForksRound2 = 3;
			break;
			case 3:
			forkSwitch24();
			checkedForksRound2 = 0;
			break;
		}

		if (switchForkIndex == 2) {
			if (checkedForks[switchForkIndex])
				disableForkSwitch1();
			else
				enableForkSwitch1();

			checkedForks [switchForkIndex] = !checkedForks [switchForkIndex];
		}
	}

	private void forkSwitch11()
	{
		routeTable[0].pointB = Const.NULLp;
		routeTable[2].pointB = routeTable[3].curPoint;
		
		updatePointsWagon (routeTable [0].curPoint, 0);
		updatePointsWagon (routeTable [2].curPoint, 2);
	}
	
	private void forkSwitch12()
	{
		routeTable[2].pointB = Const.NULLp;
		routeTable[6].pointA = routeTable[5].curPoint;
		
		updatePointsWagon (routeTable [2].curPoint, 2);
		updatePointsWagon (routeTable [6].curPoint, 6);
	}
	
	private void forkSwitch13()
	{
		routeTable[6].pointA = Const.NULLp;
		//routeTable[10].pointA = routeTable[9].curPoint ;
		
		updatePointsWagon (routeTable [6].curPoint, 6);
		//updatePointsWagon (routeTable [10].curPoint, 10);
	}
	
	private void forkSwitch14()
	{
		routeTable[0].pointB = routeTable[1].curPoint;
		
		updatePointsWagon (routeTable [0].curPoint, 0);
	}

	private void forkSwitch21()
	{
		routeTable [5].pointB = Const.NULLp;
		routeTable [7].pointB = routeTable[8].curPoint;
		updatePointsWagon (routeTable [5].curPoint, 5);
		updatePointsWagon (routeTable [7].curPoint, 7);
	}

	private void forkSwitch22()
	{
		routeTable[7].pointB = Const.NULLp;
		routeTable[15].pointA = routeTable[14].curPoint;
		updatePointsWagon (routeTable [7].curPoint, 7);
		updatePointsWagon (routeTable [15].curPoint, 15);
	}

	private void forkSwitch23()
	{
		routeTable [15].pointA = Const.NULLp;
		routeTable[10].pointA = routeTable[9].curPoint;
		updatePointsWagon (routeTable [15].curPoint, 15);
		updatePointsWagon (routeTable [10].curPoint, 10);
	}

	private void forkSwitch24()
	{
		routeTable[10].pointA = Const.NULLp;
		routeTable [5].pointB = routeTable[6].curPoint;
		updatePointsWagon (routeTable [10].curPoint, 10);
		updatePointsWagon (routeTable [5].curPoint, 5);
	}

	private void disableForkSwitch1()
	{
		routeTable [15].pointB = Const.NULLp;
		routeTable [12].pointB = routeTable [13].curPoint;
		routeTable [18].pointA = routeTable [13].curPoint;
		//routeTable [10].pointB = routeTable [9].curPoint;
		//routeTable [8].pointA = routeTable [9].curPoint;
		
		updatePointsWagon(routeTable[15].curPoint,15);
		updatePointsWagon(routeTable[12].curPoint,12);
		updatePointsWagon(routeTable[18].curPoint,18);
		//updatePointsWagon(routeTable[8].curPoint,8);
	}

	private void enableForkSwitch1()
	{
		routeTable [12].pointB = Const.NULLp;
		routeTable [15].pointB = routeTable [16].curPoint;
		routeTable [18].pointA = routeTable [17].curPoint;
		//routeTable [10].pointB = routeTable [9].curPoint;
		//routeTable [8].pointA = routeTable [9].curPoint;
		
		updatePointsWagon(routeTable[12].curPoint,12);
		updatePointsWagon(routeTable[15].curPoint,15);
		updatePointsWagon(routeTable[18].curPoint,18);
	}

}
