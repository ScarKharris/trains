﻿using UnityEngine;
using System.Collections;

public class WagonTutor : MonoBehaviour {
	private Level1Control test_level_ctrl;
    public GameScoreControl gameScoreControl;	// управление очками
    private bool isMoved;
    public TutorAnimEvent tutorAnimEvent;
	private PointLightTgtTutor tgtPntScript;
	// Use this for initialization
	void Start () {
		test_level_ctrl = GameObject.Find("gameControl").GetComponent<Level1Control>();
		tgtPntScript = gameObject.GetComponent<PointLightTgtTutor> ();
		isMoved = false;
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// если вагон перемещали - проверяем его положение
		if (isMoved) {
			if (transform.position == test_level_ctrl.points[0].curPoint.position)
			{
				test_level_ctrl.updateRouteAndHelpData(0);
				isMoved = false;
                tutorAnimEvent.on_nextButton_tutorMoveWagon();

			}

			if (transform.position == test_level_ctrl.points[1].curPoint.position)
			{
				test_level_ctrl.updateRouteAndHelpData(2);
				isMoved = false;
			}

			if (transform.position == test_level_ctrl.points[3].curPoint.position)
			{
				test_level_ctrl.updateRouteAndHelpData(4);
				isMoved = false;
			}
            if (gameScoreControl != null)
                gameScoreControl.stepsIncrease();
		}
	}

	void OnMouseDown ()
	{  
		test_level_ctrl.updateRouteAndHelpData(5);

		/*if (m_asi.IsTag ("5")) {
			m_handAnim.SetTrigger("AnimGoTo6");
		}*/
	}

	void OnMouseUp ()
	{
		tgtPntScript.scriptOn = true;
		isMoved = true;
	}
}
