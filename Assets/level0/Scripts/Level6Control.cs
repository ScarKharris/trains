﻿using UnityEngine;
using System.Collections;

public class Level6Control : BaseControl {

    public int countTargetWagonLoko1;
    public Transform[] tgtPoints1; // массив целевых точек, т.е. точки, на которые надо поставить вагоны
    public GameObject obj_loko2;
    public int countTargetWagonLoko2;
    public Animator anim_loko2;
    public Animator anim_Camera2;
    bool fl_endLevel = false;
    protected byte countTgtPnt2;	// количество достигнутых целевых точек
    public Transform[] tgtPoints2; // массив целевых точек, т.е. точки, на которые надо поставить вагоны

    private readonly Vector3 baseScale = new Vector3(1, -1, 1);
	// Use this for initialization
	void Start () {
		init ();
      
       
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	protected void init()
	{
        fl_endLevel = false;
		initTables ();
		// ставим вагончики в исходную позицию
		wagons[0].transform.position = routeTable[0].curPoint;
		wagons [0].GetComponent<Wagon> ().setCurPoints (routeTable [0]);
		wagons[1].transform.position = routeTable[1].curPoint;
		wagons [1].GetComponent<Wagon> ().setCurPoints (routeTable [1]);
		wagons[2].transform.position = routeTable[2].curPoint;
		wagons [2].GetComponent<Wagon> ().setCurPoints (routeTable [2]);
		wagons[3].transform.position = routeTable[3].curPoint;
		wagons [3].GetComponent<Wagon> ().setCurPoints (routeTable [3]);
		wagons[4].transform.position = routeTable[4].curPoint;
		wagons [4].GetComponent<Wagon> ().setCurPoints (routeTable [4]);
		wagons[5].transform.position = routeTable[5].curPoint;
		wagons [5].GetComponent<Wagon> ().setCurPoints (routeTable [5]);
		wagons[6].transform.position = routeTable[6].curPoint;
		wagons [6].GetComponent<Wagon> ().setCurPoints (routeTable [6]);
		wagons[7].transform.position = routeTable[7].curPoint;
		wagons [7].GetComponent<Wagon> ().setCurPoints (routeTable [7]);
		wagons[8].transform.position = routeTable[8].curPoint;
		wagons [8].GetComponent<Wagon> ().setCurPoints (routeTable [8]);
		wagons[9].transform.position = routeTable[27].curPoint;
		wagons [9].GetComponent<Wagon> ().setCurPoints (routeTable [27]);
		wagons[10].transform.position = routeTable[13].curPoint;
		wagons [10].GetComponent<Wagon> ().setCurPoints (routeTable [13]);
		wagons[11].transform.position = routeTable[15].curPoint;
		wagons [11].GetComponent<Wagon> ().setCurPoints (routeTable [15]);
		wagons[12].transform.position = routeTable[16].curPoint;
		wagons [12].GetComponent<Wagon> ().setCurPoints (routeTable [16]);
		wagons[13].transform.position = routeTable[17].curPoint;
		wagons [13].GetComponent<Wagon> ().setCurPoints (routeTable [17]);
		wagons[14].transform.position = routeTable[18].curPoint;
		wagons [14].GetComponent<Wagon> ().setCurPoints (routeTable [18]);
		wagons[15].transform.position = routeTable[19].curPoint;
		wagons [15].GetComponent<Wagon> ().setCurPoints (routeTable [19]);
		wagons[16].transform.position = routeTable[20].curPoint;
		wagons [16].GetComponent<Wagon> ().setCurPoints (routeTable [20]);
		wagons[17].transform.position = routeTable[21].curPoint;
		wagons [17].GetComponent<Wagon> ().setCurPoints (routeTable [21]);
		wagons[18].transform.position = routeTable[22].curPoint;
		wagons [18].GetComponent<Wagon> ().setCurPoints (routeTable [22]);
		wagons[19].transform.position = routeTable[9].curPoint;
		wagons [19].GetComponent<Wagon> ().setCurPoints (routeTable [9]);

		checkedForks = new bool[2];
		checkedForks [0] = true;
		checkedForks [1] = true;
        
        for(int i =0; i<wagons.Length;i++)
        {
             wagons[i].GetComponent<Wagon>().checkWagonTgt();
        }
	}

	override public void switchFork(byte switchForkIndex)
	{	
		switch (switchForkIndex) {
		case 0:
			if (checkedForks [switchForkIndex])
				disableForkSwitch1 ();
			else
				enableForkSwitch1 ();
			break;
		case 1:
			if (checkedForks [switchForkIndex])
				disableForkSwitch2 ();
			else
				enableForkSwitch2 ();
			break;
		}
		checkedForks [switchForkIndex] = !checkedForks [switchForkIndex];
	}

	private void enableForkSwitch1()
	{
		routeTable [3].pointB = routeTable[4].curPoint;
		routeTable [23].pointA = Const.NULLp;
		routeTable [27].pointA = routeTable[4].curPoint;
		routeTable [13].pointB = routeTable [14].curPoint;
		routeTable [15].pointA = routeTable [14].curPoint;
		routeTable [24].pointB = Const.NULLp;
		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[23].curPoint,23);
		updatePointsWagon(routeTable[27].curPoint,27);
		updatePointsWagon(routeTable[13].curPoint,13);
		updatePointsWagon(routeTable[15].curPoint,15);
		updatePointsWagon(routeTable[24].curPoint,24);
	}

	private void disableForkSwitch1()
	{
		routeTable [3].pointB = routeTable[23].curPoint;
		routeTable [23].pointA = routeTable[3].curPoint;
		routeTable [27].pointA = Const.NULLp;
		routeTable [13].pointB = Const.NULLp;
		routeTable [15].pointA = routeTable [24].curPoint;
		routeTable [24].pointB = routeTable [15].curPoint;
		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[23].curPoint,23);
		updatePointsWagon(routeTable[27].curPoint,27);
		updatePointsWagon(routeTable[13].curPoint,13);
		updatePointsWagon(routeTable[15].curPoint,15);
		updatePointsWagon(routeTable[24].curPoint,24);
	}

	private void enableForkSwitch2()
	{
		routeTable [7].pointB = routeTable[8].curPoint;
		routeTable [25].pointA = Const.NULLp;
		routeTable [9].pointA = routeTable[8].curPoint;
		routeTable [18].pointB = routeTable [19].curPoint;
		routeTable [20].pointA = routeTable [19].curPoint;
		routeTable [26].pointB = Const.NULLp;
		updatePointsWagon(routeTable[7].curPoint,7);
		updatePointsWagon(routeTable[25].curPoint,25);
		updatePointsWagon(routeTable[9].curPoint,9);
		updatePointsWagon(routeTable[18].curPoint,18);
		updatePointsWagon(routeTable[20].curPoint,20);
		updatePointsWagon(routeTable[26].curPoint,26);
	}

	private void disableForkSwitch2()
	{
		routeTable [7].pointB = routeTable[25].curPoint;
		routeTable [25].pointA = routeTable[7].curPoint;
		routeTable [9].pointA = Const.NULLp;
		routeTable [18].pointB = Const.NULLp;
		routeTable [20].pointA = routeTable [26].curPoint;
		routeTable [26].pointB = routeTable [20].curPoint;
		updatePointsWagon(routeTable[7].curPoint,7);
		updatePointsWagon(routeTable[25].curPoint,25);
		updatePointsWagon(routeTable[9].curPoint,9);
		updatePointsWagon(routeTable[18].curPoint,18);
		updatePointsWagon(routeTable[20].curPoint,20);
		updatePointsWagon(routeTable[26].curPoint,26);
	}


    override public void enterWagonTgtPnt()	// ва
    {
        Debug.Log("Встал на целевую точку!");
        countTgtPnt++;
        if (countTgtPnt >= countTargetWagonLoko1)
        {
            Debug.Log(countTgtPnt);
            int fl_onTrgtPoint = 0;

            for (int w = 0; w < countTargetWagonLoko1; w++)
            {
                for (int t = 0; t < tgtPoints1.Length; t++)
                {
                    if ((wagons[w].transform.position == tgtPoints1[t].transform.position))
                    {
                        Debug.Log("222!");
                      
                        fl_onTrgtPoint++;
                    }
                }
            }
            Debug.Log(fl_onTrgtPoint);
            if (fl_onTrgtPoint >= countTargetWagonLoko1)
            {
                //привязка вагонов к поезду(вагоны будут дочерними обьектами поезда), так как анимация только у поезда
                for (int i = 0; i < countTargetWagonLoko1; i++)
                {
                    wagons[i].transform.parent = obj_loko.transform;
                    wagons[i].transform.localScale = baseScale;
                }
                if (fl_endLevel)
                {
                    //включение анимации камеры
                    anim_Camera.GetComponent<Animator>().enabled = true;
                    //показ текста  
                    panelManagerLevel.showPanelLevelCompletePre();

                    anim_loko.GetComponent<eventFinalWindow>().fl_lastTrain = true;
                }
                else
                    anim_loko.GetComponent<eventFinalWindow>().fl_lastTrain = false;
                
                //включение анимации поезда
                anim_loko.SetBool("start", true);
               
                   
                fl_endLevel = true;
                Debug.Log(fl_onTrgtPoint);
                Debug.Log(fl_endLevel);
                Debug.Log("Все вагоны расставлены11111!");
            }





            fl_onTrgtPoint = 0;
          
            for (int w = countTargetWagonLoko1; w < countTargetWagonLoko1 + countTargetWagonLoko2; w++)
            {
               for (int t = 0; t < tgtPoints2.Length; t++)
                {
                    if ((wagons[w].transform.position == tgtPoints2[t].transform.position))
                    {
                        Debug.Log("333!");
                        
                        fl_onTrgtPoint++;
                    }
                }
            }
            if (fl_onTrgtPoint == countTargetWagonLoko2)
            {
                //привязка вагонов к поезду(вагоны будут дочерними обьектами поезда), так как анимация только у поезда
                for (int i = countTargetWagonLoko1; i < wagons.Length; i++)
                {
                    wagons[i].transform.parent = obj_loko2.transform;
                    wagons[i].transform.localScale = baseScale;
                }
                if (fl_endLevel)
                {
                    //включение анимации камеры
                    anim_Camera.GetComponent<Animator>().enabled = true;
                    //показ текста  
                    panelManagerLevel.showPanelLevelCompletePre();

                    anim_loko2.GetComponent<eventFinalWindow>().fl_lastTrain = true;
                }
                else
                    anim_loko2.GetComponent<eventFinalWindow>().fl_lastTrain = false;
            
                anim_loko2.SetBool("start", true);
               
                 
                fl_endLevel = true;
                Debug.Log("Все вагоны расставлены22222!");
            }
        }
    }
 



}
