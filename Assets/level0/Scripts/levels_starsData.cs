﻿using UnityEngine;
using System.Collections;

public struct level_starsData{
	public  int countStepStar1;
	public  int countStepStar2;
	public  int countStepStar3;
}

//public level_starsData global::Level1_starsData ; 

public static class Level1_starsData{

	public static int countStepStar1 = 10;
	public static int countStepStar2 = 8;
	public static int countStepStar3 = 7;
}

public static class Level2_starsData{
	
	public static int countStepStar1 = 10;
	public static int countStepStar2 = 8;
	public static int countStepStar3 = 7;
}

public static class Level3_starsData
{

    public static int countStepStar1 = 20;
    public static int countStepStar2 = 16;
    public static int countStepStar3 = 13;
}
public static class Level4_starsData
{

    public static int countStepStar1 = 11;
    public static int countStepStar2 = 9;
    public static int countStepStar3 = 8;
}
public static class Level5_starsData
{

    public static int countStepStar1 = 65;
    public static int countStepStar2 = 53;
    public static int countStepStar3 = 49;
}

public static class Level6_starsData
{

    public static int countStepStar1 = 30;
    public static int countStepStar2 = 23;
    public static int countStepStar3 = 16;
}

public static class Level7_starsData
{

    public static int countStepStar1 = 33;
    public static int countStepStar2 = 25;
    public static int countStepStar3 = 20;
}