﻿using UnityEngine;
using System.Collections;

public class PointLightGo : MonoBehaviour {
	public GameObject pointLightGoGlobal;		//внешняя ссылка на префаб
	private GameObject pointLightGoClone; //внутренняя ссылка на префаб


	public void createPointLightGo()
	{
		pointLightGoClone = (GameObject)Instantiate (pointLightGoGlobal,transform.position,new Quaternion());

	}
}
