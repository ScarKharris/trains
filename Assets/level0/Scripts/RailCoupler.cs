﻿using UnityEngine;
using System.Collections;

public class RailCoupler : MonoBehaviour {
	private Animator animator;
	bool isConnected;
	// Use this for initialization
	void Start () {
		isConnected = true;
		animator = GetComponent <Animator>();
		animator.speed = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	// команда начала процесса сцепления/расцепления рельс
	public void startProcess()
	{
		animator.speed = 1;
	}
	// команда конца процесса сцепления/расцепления рельс
	public void finishedProcess()
	{
		animator.speed = 0;
	}
}
