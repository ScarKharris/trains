﻿using UnityEngine;
using System.Collections;

public class Level1Control : BaseControl {
	public Animator m_handAnim;
	private AnimatorStateInfo m_asi;
	public GameObject m_pointLightHelp;
	private Transform m_transformPointLightHelp;	// положение вспомогательной точки
	private Vector3 m_switchForkPosition;
	private TutorAnimEvent m_ae;
    public PanelManagerLevelTutor panelManagerLevelTutor;
	BoxCollider m_bc1, m_bc2, m_forkSwitch;
	// Use this for initialization
	void Start () {
		init ();
        panelManagerLevelTutor = GameObject.Find("menuManager").GetComponent<PanelManagerLevelTutor>();
		m_transformPointLightHelp = m_pointLightHelp.GetComponent<Transform>();	//transform = test_level_ctrl.points [6].curPoint.position;
		m_transformPointLightHelp.position = routeTable [6].curPoint;
		m_switchForkPosition = GameObject.Find("PointFork").GetComponent<Transform>().position;

		m_ae = Camera.main.GetComponent<TutorAnimEvent>();	// чтобы сообщить tutorAnimEvent о событии

	}

	// Update is called once per frame
	void Update () {
	
	}

	// переключение стрелки switchPoint - точка, на которой находится стрелка
	override public void switchFork(byte switchForkIndex)
	{	
		if (checkedForks[0])
			disableForkSwitch1();
		else
			enableForkSwitch1();
		checkedForks [0] = !checkedForks [0];
	/*	switch (switchForkIndex) {
		case 0:
			if (checkedForks[switchForkIndex])
				disableForkSwitch1();
			else
				enableForkSwitch1();
			break;
		case 1:
			if (checkedForks[switchForkIndex])
				disableForkSwitch2();
			else
				enableForkSwitch2();
			break;
		}
		checkedForks [switchForkIndex] = !checkedForks [switchForkIndex];*/
	}

	protected void init()
	{
		initTables ();		
		// ставим вагончики в исходную позицию
		wagons[0].transform.position = routeTable[6].curPoint;
		wagons [0].GetComponent<Wagon> ().setCurPoints (routeTable [6]);
		wagons[1].transform.position = routeTable[2].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[2]);

		checkedForks = new bool[1];
		checkedForks [0] = true;

		m_bc1 = wagons[0].GetComponent<BoxCollider>();
		m_bc2 = wagons[1].GetComponent<BoxCollider>();
		m_bc2.enabled = false;
		m_forkSwitch = GameObject.Find("Switch").GetComponent<BoxCollider>();
	}

	private void enableForkSwitch1()
	{
		
		routeTable [0].pointB = Const.NULLp;
		routeTable [2].pointB = routeTable [3].curPoint;
		routeTable [3].pointB = routeTable [4].curPoint;
		routeTable [4].pointA = routeTable [3].curPoint;

		updatePointsWagon(routeTable[0].curPoint,0);
		updatePointsWagon(routeTable[2].curPoint,2);
		updatePointsWagon(routeTable[3].curPoint,3);
		updatePointsWagon(routeTable[4].curPoint,4);

//		GameObject.Find("TrafficLight 5/Sphere Green").GetComponent<Renderer>().material = matGreen;
//		GameObject.Find("TrafficLight 5/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 5/Sphere Green/Point light").SetActive (true);
//		GameObject.Find("TrafficLight 5/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 5/Sphere Red/Point light").SetActive (false);

	//	GameObject.Find("TrafficLight 4/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 4/Sphere Green/Point light").SetActive (false);
	//	GameObject.Find("TrafficLight 4/Sphere Red").GetComponent<Renderer>().material  = matRed;
	//	GameObject.Find("TrafficLight 4/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 4/Sphere Red/Point light").SetActive (true);

	//	GameObject.Find("TrafficLight 3/Sphere Green").GetComponent<Renderer>().material  = matGreen;
	//	GameObject.Find("TrafficLight 3/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 3/Sphere Green/Point light").SetActive (true);
	//	GameObject.Find("TrafficLight 3/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 3/Sphere Red/Point light").SetActive (false);
	}

	private void disableForkSwitch1()
	{
		routeTable [0].pointB = routeTable[1].curPoint;
		routeTable [4].pointA = routeTable[1].curPoint;
		routeTable [2].pointB = Const.NULLp;


		updatePointsWagon(routeTable[0].curPoint,0);
		updatePointsWagon(routeTable[4].curPoint,4);
		updatePointsWagon(routeTable[2].curPoint,2);

//		GameObject.Find("TrafficLight 5/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 5/Sphere Green/Point light").SetActive (false);
//		GameObject.Find("TrafficLight 5/Sphere Red").GetComponent<Renderer>().material  = matRed;
//		GameObject.Find("TrafficLight 5/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 5/Sphere Red/Point light").SetActive (true);
		
	//	GameObject.Find("TrafficLight 4/Sphere Green").GetComponent<Renderer>().material  = matGreen;
	//	GameObject.Find("TrafficLight 4/TrafficLightBack").GetComponent<Renderer>().material  = matBackGreen;
//		GameObject.Find ("TrafficLight 4/Sphere Green/Point light").SetActive (true);
	//	GameObject.Find("TrafficLight 4/Sphere Red").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 4/Sphere Red/Point light").SetActive (false);
		
	//	GameObject.Find("TrafficLight 3/Sphere Green").GetComponent<Renderer>().material  = matOff;
//		GameObject.Find ("TrafficLight 3/Sphere Green/Point light").SetActive (false);
	//	GameObject.Find("TrafficLight 3/Sphere Red").GetComponent<Renderer>().material  = matRed;
	//	GameObject.Find("TrafficLight 3/TrafficLightBack").GetComponent<Renderer>().material  = matBackRed;
//		GameObject.Find ("TrafficLight 3/Sphere Red/Point light").SetActive (true);
	}
	// целевая точка постоянно переносится. В данном скрипте она используется для формирования событий 
	override public void enterWagonTgtPnt()
	{   
        countTgtPnt++;
        if (countTgtPnt == wagons.Length)
        {

            //привязка вагонов к поезду(вагоны будут дочерними обьектами поезда), так как анимация только у поезда
            for (int i = 0; i < wagons.Length; i++)
                wagons[i].transform.parent = obj_loko.transform;
            //включение анимации камеры
            anim_Camera.GetComponent<Animator>().enabled = true;
            //включение анимации поезда
            anim_loko.enabled = true;
            //показ текста  
            panelManagerLevelTutor.showPanelLevelCompletePre();
			m_pointLightHelp.SetActive(false);
            m_handAnim.gameObject.SetActive(false);
			m_ae.final();
        }
	}

	/*override public void exitWagonTgtPnt()	// ва
	{

	}*/

	public void updateRouteAndHelpData(byte index)
	{
		m_asi = m_handAnim.GetCurrentAnimatorStateInfo(0);
		switch (index) 
		{
			case 0:	// шаг 1
				if (m_asi.IsTag ("1")){
				routeTable[0].pointA = Const.NULLp;	// убираем возможность вернуть вагон на предыдущую точку
				updatePointsWagon(routeTable[0].curPoint, 0); // обновляем информацию о точках у вагона
				m_forkSwitch.enabled = true;
				m_bc1.enabled = false;
				m_transformPointLightHelp.position = m_switchForkPosition;	// переключаем pointLightHelp	
				m_handAnim.SetTrigger("AnimGoTo2");	// переключаем анимацию руки
				m_ae.on_tutorWagonMove();	// переключаем панельку
				}
				break;
			case 1:	// шаг 2
				m_bc1.enabled = true;
				m_forkSwitch.enabled = false;
				m_transformPointLightHelp.position = routeTable[0].curPoint;
				m_handAnim.SetTrigger ("AnimGoTo3");
				m_ae.on_tutorSwitchFork();
				break;
			case 2:	// шаг 3
				m_bc1.enabled = false;
				m_forkSwitch.enabled = true;
				m_transformPointLightHelp.position = m_switchForkPosition;
				m_handAnim.SetTrigger("AnimGoTo2");	// переключаем анимацию руки
				m_ae.on_tutorWagonMove();	// переключаем панельку
				break;
			case 3:	// шаг 4
				m_bc2.enabled = true;
				m_forkSwitch.enabled = false;
				m_transformPointLightHelp.position = routeTable[2].curPoint;
				m_handAnim.SetTrigger ("AnimGoTo4");
				m_ae.on_tutorSecondSwitchFork();	// переключаем панельку
				break;
			case 4: // шаг 5
				routeTable[3].pointA = Const.NULLp;	// убираем возможность вернуть вагон на предыдущую точку
				updatePointsWagon(routeTable[3].curPoint, 3); // обновляем информацию о точках у вагона
				m_transformPointLightHelp.position = routeTable[3].curPoint;
				m_handAnim.SetTrigger("AnimGoTo6");	// переключаем анимацию руки
				m_ae.on_tutorMoveConnectedWags();
				break;
			case 5:
				if (m_asi.IsTag ("0")) {
					//m_ae.on_tutorPressHoldWagon();
					m_handAnim.SetTrigger("AnimGoTo1");
				}
				if (m_asi.IsTag ("4")) {
					m_handAnim.SetTrigger("AnimGoTo5");
				}
				break;
			case 6:
				if (m_asi.IsTag ("5")) {
					m_handAnim.SetTrigger("AnimGoTo6");
				}
				break;
		}
	}

	public void setPointLightGoPosition(byte index)
	{
		switch (index)
		{
		case 0:	// шаг 1
			routeTable [0].pointA = Const.NULLp;	// убираем возможность вернуть вагон на предыдущую точку
			updatePointsWagon (routeTable [0].curPoint, 0); // обновляем информацию о точках у вагона
			m_forkSwitch.enabled = true;
			m_bc1.enabled = false;
			break;
		}
	}
}