﻿using UnityEngine;
using System.Collections;

public class Level3Control : BaseControl {

    private readonly Vector3 baseScale = new Vector3(1, -1, 1);
	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	protected void init()
	{
		initTables ();		
		// ставим вагончики в исходную позицию

		wagons[0].transform.position = routeTable[6].curPoint;
		wagons[0].GetComponent<Wagon>().setCurPoints (routeTable[6]);
		wagons[1].transform.position = routeTable[4].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[4]);
		wagons[2].transform.position = routeTable[5].curPoint;
		wagons[2].GetComponent<Wagon>().setCurPoints(routeTable[5]);
		wagons[1].transform.rotation = Quaternion.Euler (270, 39.69955f, 0);
		wagons[2].transform.rotation = Quaternion.Euler (270, 39.69955f, 0);

		checkedForks = new bool[1];
		checkedForks [0] = true;
	}

	override public void switchFork(byte switchForkIndex)
	{	
		if (checkedForks [0])
			disableForkSwitch1 ();
		else
			enableForkSwitch1 ();
		checkedForks [0] = !checkedForks [0];
	}

	private void enableForkSwitch1()
	{
		routeTable [0].pointB = routeTable [1].curPoint;
		routeTable [4].pointA = Const.NULLp;
		routeTable [2].pointA = routeTable [1].curPoint;
		routeTable [3].pointA = Const.NULLp;
		
		updatePointsWagon (routeTable [0].curPoint, 0);
		updatePointsWagon (routeTable [4].curPoint, 4);
		updatePointsWagon (routeTable [2].curPoint, 2);
		updatePointsWagon (routeTable [3].curPoint, 3);
	}

	private void disableForkSwitch1()
	{
		routeTable [0].pointB = routeTable [3].curPoint;
		routeTable [4].pointA = routeTable [3].curPoint;
		routeTable [2].pointA = Const.NULLp;
		routeTable [3].pointA = routeTable [0].curPoint;

		updatePointsWagon (routeTable [0].curPoint, 0);
		updatePointsWagon (routeTable [4].curPoint, 4);
		updatePointsWagon (routeTable [2].curPoint, 2);
		updatePointsWagon (routeTable [3].curPoint, 3);
	}

	override public void enterWagonTgtPnt()
	{
		if ((wagons [0].transform.position == tgtPoints [2].position) && (wagons [1].transform.position == tgtPoints [1].position) &&
		    (wagons [2].transform.position == tgtPoints [0].position)) 
		{

            //привязка вагонов к поезду(вагоны будут дочерними обьектами поезда), так как анимация только у поезда
            for (int i = 0; i < wagons.Length; i++)
            {
                wagons[i].transform.parent = obj_loko.transform;
                wagons[i].transform.localScale = baseScale;
            }
            //включение анимации камеры
            anim_Camera.GetComponent<Animator>().enabled = true;
            //включение анимации поезда
            anim_loko.SetBool("start", true);
            //показ текста  
            panelManagerLevel.showPanelLevelCompletePre();

			Debug.Log("Вагоны расставлены");
		}
	}
}
