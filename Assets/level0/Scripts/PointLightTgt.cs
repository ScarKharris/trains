﻿using UnityEngine;
using System.Collections;

public class PointLightTgt : MonoBehaviour {
	public GameObject[] m_pointLightTgt;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {

	}

	void OnMouseDown()
	{
		switchPointLightTgt (true);
	}

	void OnMouseUp()
	{
		switchPointLightTgt (false);
	}

	void switchPointLightTgt(bool mode)
	{
		foreach (GameObject PointLightTgt in m_pointLightTgt) 
			PointLightTgt.SetActive (mode);
	}
}
