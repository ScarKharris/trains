﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

public class Level7Control : BaseControl {

	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected void init()
	{
		initTables ();
		wagons[0].transform.position = routeTable[0].curPoint;
		wagons[0].GetComponent<Wagon>().setCurPoints (routeTable[0]);
		wagons[1].transform.position = routeTable[1].curPoint;
		wagons[1].GetComponent<Wagon>().setCurPoints (routeTable[1]);
		wagons[2].transform.position = routeTable[15].curPoint;
		wagons[2].GetComponent<Wagon>().setCurPoints (routeTable[15]);
		wagons[3].transform.position = routeTable[16].curPoint;
		wagons[3].GetComponent<Wagon>().setCurPoints (routeTable[16]);
		wagons[4].transform.position = routeTable[21].curPoint;
		wagons[4].GetComponent<Wagon>().setCurPoints (routeTable[21]);
		wagons[5].transform.position = routeTable[22].curPoint;
		wagons[5].GetComponent<Wagon>().setCurPoints (routeTable[22]);
		wagons[6].transform.position = routeTable[25].curPoint;
		wagons[6].GetComponent<Wagon>().setCurPoints (routeTable[25]);
		wagons[7].transform.position = routeTable[26].curPoint;
		wagons[7].GetComponent<Wagon>().setCurPoints (routeTable[26]);
		checkedForks = new bool[3];
		checkedForks [0] = true;
		checkedForks [1] = true;
		checkedForks [2] = true;
	}

	override public void switchFork(byte switchForkIndex)
	{	
		switch (switchForkIndex) {
		case 0:
			if (checkedForks[switchForkIndex])
				disableForkSwitch1();
			else
				enableForkSwitch1();
			break;
		case 1:
			if (checkedForks[switchForkIndex])
				disableForkSwitch2();
			else
				enableForkSwitch2();
			break;
		case 2:
			if (checkedForks[switchForkIndex])
				disableForkSwitch3();
			else
				enableForkSwitch3();
			break;
		}
		checkedForks [switchForkIndex] = !checkedForks [switchForkIndex];
	}
	
	private void enableForkSwitch1()
	{
		routeTable [1].pointB = routeTable [2].curPoint;
		routeTable [16].pointB = routeTable [17].curPoint;
		routeTable [3].pointA = routeTable [2].curPoint;
		routeTable [22].pointB = routeTable [23].curPoint;
		routeTable [18].pointA = routeTable [17].curPoint;
		routeTable [26].pointB = Const.NULLp;
		routeTable [24].pointA = routeTable [23].curPoint;

		updatePointsWagon (routeTable [1].curPoint, 1);
		updatePointsWagon (routeTable [16].curPoint, 16);
		updatePointsWagon (routeTable [3].curPoint, 3);
		updatePointsWagon (routeTable [22].curPoint, 22);
		updatePointsWagon (routeTable [18].curPoint, 18);
		updatePointsWagon (routeTable [26].curPoint, 26);
		updatePointsWagon (routeTable [24].curPoint, 24);
	}
	
	private void disableForkSwitch1()
	{
		routeTable [1].pointB = Const.NULLp;
		routeTable [16].pointB = routeTable [27].curPoint;
		routeTable [3].pointA = routeTable [27].curPoint;
		routeTable [22].pointB = routeTable [28].curPoint;
		routeTable [18].pointA = routeTable [28].curPoint;
		routeTable [26].pointB = routeTable [29].curPoint;
		routeTable [24].pointA = routeTable [29].curPoint;

		updatePointsWagon (routeTable [1].curPoint, 1);
		updatePointsWagon (routeTable [16].curPoint, 16);
		updatePointsWagon (routeTable [3].curPoint, 3);
		updatePointsWagon (routeTable [22].curPoint, 22);
		updatePointsWagon (routeTable [18].curPoint, 18);
		updatePointsWagon (routeTable [26].curPoint, 26);
		updatePointsWagon (routeTable [24].curPoint, 24);
	}

	private void enableForkSwitch2()
	{
		routeTable [3].pointB = routeTable [4].curPoint;
		routeTable [5].pointA = routeTable [4].curPoint;
		routeTable [18].pointB = routeTable [19].curPoint;
		routeTable [20].pointA = routeTable [19].curPoint;
		routeTable [24].pointB = Const.NULLp;
		
		updatePointsWagon (routeTable [3].curPoint, 3);
		updatePointsWagon (routeTable [5].curPoint, 5);
		updatePointsWagon (routeTable [18].curPoint, 18);
		updatePointsWagon (routeTable [20].curPoint, 20);
		updatePointsWagon (routeTable [24].curPoint, 24);
	}

	private void disableForkSwitch2()
	{
		routeTable [3].pointB = Const.NULLp;
		routeTable [5].pointA = routeTable [30].curPoint;
		routeTable [18].pointB = routeTable [30].curPoint;
		routeTable [20].pointA = routeTable [31].curPoint;
		routeTable [24].pointB = routeTable [31].curPoint;

		updatePointsWagon (routeTable [3].curPoint, 3);
		updatePointsWagon (routeTable [5].curPoint, 5);
		updatePointsWagon (routeTable [18].curPoint, 18);
		updatePointsWagon (routeTable [20].curPoint, 20);
		updatePointsWagon (routeTable [24].curPoint, 24);
	}

	private void enableForkSwitch3()
	{
		routeTable [5].pointB = routeTable [6].curPoint;
		routeTable [7].pointA = routeTable [6].curPoint;
		routeTable [20].pointB = Const.NULLp;
		
		updatePointsWagon (routeTable [5].curPoint, 5);
		updatePointsWagon (routeTable [7].curPoint, 7);
		updatePointsWagon (routeTable [20].curPoint, 20);
	}

	private void disableForkSwitch3()
	{
		routeTable [5].pointB = Const.NULLp;
		routeTable [7].pointA = routeTable [32].curPoint;
		routeTable [20].pointB = routeTable [32].curPoint;

		updatePointsWagon (routeTable [5].curPoint, 5);
		updatePointsWagon (routeTable [7].curPoint, 7);
		updatePointsWagon (routeTable [20].curPoint, 20);
	}
}
