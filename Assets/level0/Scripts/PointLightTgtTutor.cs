﻿using UnityEngine;
using System.Collections;

public class PointLightTgtTutor : MonoBehaviour {

	public GameObject[] m_pointLightTgt;
	public bool scriptOn;	// вкл/выкл работу подсветки
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
	}
	
	void OnMouseDown()
	{ 
		if (scriptOn)
			switchPointLightTgt (true);
	}
	
	void OnMouseUp()
	{
		if (scriptOn)
			switchPointLightTgt (false);
	}
	
	void switchPointLightTgt(bool mode)
	{
		foreach (GameObject PointLightTgt in m_pointLightTgt) 
			PointLightTgt.SetActive (mode);
	}
}
