﻿using UnityEngine;
using System.Collections;

public class eventFinalWindow : MonoBehaviour {

	public ParticleSystem partical;
	public PanelManagerLevel panelManagerLevel;
    public bool fl_lastTrain = true;

	public void showWindowFinal(){

        if (fl_lastTrain)
		    panelManagerLevel.showPanelLevelCompleteFinal();
	}
	public void showFullGaz(){

		partical.startLifetime = 2;
	
		partical.startSpeed = 8;
		partical.startSize = 6;
		partical.emissionRate = 20;




	}
}
